��          \      �       �   ^   �      (  Q   1     �     �  c   �       .  '  ^   V     �  Q   �          %  c   5     �                                       Allows posts and pages to be archived so you can unpublish content without having to trash it. Archived Archived <span class="count">(%s)</span> Archived <span class="count">(%s)</span> Archived Post Status Frankie Jarrett You can't edit this item because it has been Archived. Please change the post status and try again. https://frankiejarrett.com PO-Revision-Date: 2016-08-17 04:12:55+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.3.0-alpha
Language: en_CA
Project-Id-Version: Plugins - Archived Post Status - Stable (latest release)
 Allows posts and pages to be archived so you can unpublish content without having to trash it. Archived Archived <span class="count">(%s)</span> Archived <span class="count">(%s)</span> Archived Post Status Frankie Jarrett You can't edit this item because it has been Archived. Please change the post status and try again. https://frankiejarrett.com 