<li class="col col-1-6" style="background: url('<?php echo get_the_post_thumbnail_url()?>') no-repeat center center">
                    <div class="name"><?php the_title(); ?></div>
                    <div class="likes">
                        <i data-post-id="<?= get_the_ID();?>" class="fa <?= \Face3\Nest\Filters\frontend\is_liked() ? 'fa-heart' : 'fa-heart-o'?>" aria-hidden="true"></i>&nbsp;<?= intval(get_post_meta(get_the_ID(), FACE3_NEST_FILTERS_FAVOURITE_COUNT_META_KEY, true))?>
                        <!-- <i class="fa fa-heart" aria-hidden="true"></i> -->
                    </div>
                    <div class="category">
                        <?php
                        $icon=null;
                        switch (get_post_type()) {
                            case 'creator':
                                $icon='ico-creator.svg';
                                break;
                            case 'influence':
                                $icon='ico-influence.svg';
                                break;
                            case 'report':
                                $icon='ico-report.svg';
                                break;
                        }  ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $icon ?>" alt="<?php echo ucfirst(get_post_type())?>" />
                        
                    </div>
                    <a href="<?php the_permalink(); ?>"></a>
                </li>