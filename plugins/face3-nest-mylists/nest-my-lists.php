<?php
/*
Plugin Name: Nest - "My Lists" feature
Description: Provides support for "My Lists" feature of Nest
Version: 1.0
Author: Face3
Author URI: http://face3.com
License: GPL2
Domain: face3-nest
*/

namespace Face3\Nest\MyLists;

defined('ABSPATH') || exit;

define('FACE3_NEST_MYLISTS_PLUGIN_FILE', __FILE__);

include_once 'src/frontend/ajax-functions.php';
add_action( 'wp_ajax_create_playlist', 'Face3\Nest\MyLists\frontend\create_playlist' );
add_action( 'wp_ajax_edit_playlist', 'Face3\Nest\MyLists\frontend\edit_playlist' );
add_action( 'wp_ajax_delete_playlist', 'Face3\Nest\MyLists\frontend\delete_playlist' );
add_action( 'wp_ajax_add_to_playlist', 'Face3\Nest\MyLists\frontend\add_to_playlist' );
add_action( 'wp_ajax_remove_from_playlist', 'Face3\Nest\MyLists\frontend\remove_from_playlist' );

add_action( 'wp_enqueue_scripts', function() {
	wp_register_script('nest-users', plugins_url('assets/js/users.js.php', FACE3_NEST_MYLISTS_PLUGIN_FILE ));
	wp_register_script('nest-user-suggest', plugins_url('assets/js/user-suggest.js', FACE3_NEST_MYLISTS_PLUGIN_FILE ), ['nest-users','jquery-ui-autocomplete']);
}, 0);