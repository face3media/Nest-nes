=== Face3 Nest My Lists ===
Contributors: Face3
Tags:
Requires at least: 3.5
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tested up to: 4.8.0
Stable tag: 1.0.0
Provides management functions for Nest - My lists feature.


== Description ==

Provides management functions for Nest - My lists feature.

AJAX calls:

* create_playlist, param: playlist_name
* update_playlist, param: playlist_id, playlist_name
* delete_playlist, param: playlist_id
* add_to_playlist, param: playlist_id, post_id
* remove_from_playlist, param: playlist_id, post_id

== Changelog ==
= 1.0.0 =
* Basic AJAX management functions for users' my list feature