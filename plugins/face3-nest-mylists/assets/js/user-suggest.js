;(function( $ ) {
    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }
	$(document).ready( function() {
		var position = { offset: '0, -1' };
		if ( typeof isRtl !== 'undefined' && isRtl ) {
			position.my = 'right top';
			position.at = 'right bottom';
		}
		$( '.nest-suggest-user' ).each( function(){
			var $this = $( this ),
				autocompleteField = ( typeof $this.data( 'autocompleteField' ) !== 'undefined' ) ? $this.data( 'autocompleteField' ) : 'user_login';

			$this
              .on( "keydown", function( event ) {
                  if ( event.keyCode === $.ui.keyCode.TAB &&
                    $this.autocomplete( "instance" ).menu.active ) {
                      event.preventDefault();
                  }
              })
              .autocomplete({
                  source: function( request, response) {
                      response( $.ui.autocomplete.filter(
                          autocompleteField === 'user_login' ? NEST.users.logins : NEST.users.emails, extractLast( request.term ) ) );
                  },
                  focus: function() {
                      return false;
                  },
                  search: function() {
                      // custom minLength
                      var term = extractLast( this.value );
                      if ( term.length < 2 ) {
                          return false;
                      }
                  },
                  select: function(event, ui) {
                      var terms = split( this.value );
                      // remove the current input
                      terms.pop();
                      // add the selected item
                      terms.push( ui.item.value );
                      // add placeholder to get the comma-and-space at the end
                      terms.push( "" );
                      this.value = terms.join( ", " );
                      return false;
                  },
                  delay: 0,
                  minLength: 1,
                  position:  position,
                  open: function() {
                      $( this ).addClass( 'open' );
                  },
                  close: function() {
                      $( this ).removeClass( 'open' );
                  }
			});
		});
	});
})( jQuery );