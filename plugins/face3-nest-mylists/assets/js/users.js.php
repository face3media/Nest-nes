<?php
define('DOING_AJAX', true);
header('Content-Type: application/javascript');

require_once('../../../../../wp-load.php');

if ( ! is_user_logged_in() ) {
	echo 'Unauthorized access';
	exit; // prevent unauthorized access
}

$users = get_users( array(
	'blog_id' => false
) );

foreach ( $users as $user ) {
	$user_emails[] = $user->user_email;
	$user_logins[] = $user->user_login;
}
?>
;(function(window) {
    var user_emails = <?= json_encode($user_emails);?>,
    	user_logins = <?= json_encode($user_logins);?>

	window.NEST = window.NEST || {};
	window.NEST.users = {
	    emails : user_emails,
	    logins : user_logins
	}
})(window);

<?php exit;
