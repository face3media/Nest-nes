<?php
namespace Face3\Nest\MyLists\frontend;



/**
 * Create a new playlist for the user
 * @uses $_POST['playlist_name']
 */
function create_playlist() {
	// security
	if ( ! check_ajax_referer(FACE3_NEST_FILTERS_AJAX_ACTION) ) {
		wp_send_json_error();
	}

	// filter inputs
	$name = filter_input( INPUT_POST, 'playlist_name', FILTER_SANITIZE_STRING );

	// create playlist in db
	$id = wp_insert_post( [
		'post_title' => $name,
		'post_name' => sanitize_title($name),
		'post_type' => 'playlist',
		'post_status' => 'publish',
	] );

	if ( $id ) {
		wp_send_json_success([
			'id' => $id,
			'playlist_name' => $name,
			'message' => __('Playlist created.', 'face3-nest'),
		]);
	}

	wp_send_json_error([
		'message' => __('Could not create playlist.', 'face3-nest'),
	]);
}


/**
 * Edit a playlist's name
 * @uses $_POST['playlist_id']
 * @uses $_POST['playlist_name']
 */
function edit_playlist() {
	// security
	if ( ! check_ajax_referer(FACE3_NEST_FILTERS_AJAX_ACTION) ) {
		wp_send_json_error();
	}

	// filter inputs
	$id = filter_input( INPUT_POST, 'playlist_id', FILTER_SANITIZE_NUMBER_INT );
	$name = filter_input( INPUT_POST, 'playlist_name', FILTER_SANITIZE_STRING );

	// check that playlist exists
	$playlist = get_post($id);
	if ( empty($playlist) ) {
		wp_send_json_error([
			'message' => __('Playlist not found.', 'face3-nest')
		]);
	}

	// only the owner of this playlist can edit it
	if ( get_current_user_id() !=  get_post_field('post_author', $id) ) {
		wp_send_json_error([
			'message' => __('You are not allowed to update this playlist. It belongs to someone else.', 'face3-nest')
		]);
	}

	// update playlist in db
	$success = wp_update_post( [
		'ID'           => $id,
		'post_title'   => $name,
	] );

	if ( $success ) {
		wp_send_json_success([
			'message'   => __('Playlist updated.', 'face3-nest'),
			'playlist_name' => $name,
			'id'        => $id
		]);
	}

	wp_send_json_error();
}

/**
 * Delete a playlist
 * @uses $_POST['playlist_id']
 */
function delete_playlist() {
	// security
	if ( ! check_ajax_referer(FACE3_NEST_FILTERS_AJAX_ACTION) ) {
		wp_send_json_error();
	}

	// filter input
	$id = filter_input( INPUT_POST, 'playlist_id', FILTER_SANITIZE_NUMBER_INT );

	// check that playlist exists
	$playlist = get_post($id);
	if ( empty($playlist) ) {
		wp_send_json_error([
			'message' => __('Playlist not found.', 'face3-nest')
		]);
	}

	// only the owner of this playlist can delete it
	if ( get_current_user_id() != get_post_field('post_author', $id) ) {
		wp_send_json_error([
			'message' => __('You are not allowed to delete this playlist. It belongs to someone else. You are:'.get_current_user_id().', it belongs to:'.get_post_field('post_author', $id), 'face3-nest')
		]);
	}

	// delete
	if ( wp_delete_post($id) !== false ) {
		wp_send_json_success([
			'message'   => __('Playlist deleted.', 'face3-nest'),
			'id'        => $id
		]);
	}

	wp_send_json_error();
}

/**
 * Add a post id at the beginning of a playlist.
 * @uses $_POST['playlist_id']
 * @uses $_POST['post_id']
 */
function add_to_playlist() {
// security
	if ( ! check_ajax_referer(FACE3_NEST_FILTERS_AJAX_ACTION) ) {
		wp_send_json_error();
	}

	// filter inputs
	$playlist_id = filter_input( INPUT_POST, 'playlist_id', FILTER_SANITIZE_NUMBER_INT );
	$post_id = filter_input( INPUT_POST, 'post_id', FILTER_SANITIZE_STRING );

	// check that playlist exists
	$playlist = get_post($playlist_id);
	if ( empty($playlist) ) {
		wp_send_json_error([
			'message' => __('Playlist not found.', 'face3-nest')
		]);
	}

	// only the owner of this playlist can edit it
	if ( get_current_user_id() != get_post_field('post_author', $playlist_id) ) {
		wp_send_json_error([
			'message' => __('You are not allowed to update this playlist. It belongs to someone else.', 'face3-nest')
		]);
	}

	// make sure post exists
	$post = get_post($post_id);
	if ( empty($post) ) {
		wp_send_json_error([
			'message' => __('Post not found.', 'face3-nest')
		]);
	}

	// Update and save meta
	$playlist = get_post_meta($playlist_id, 'playlist', true);
	if ( empty($playlist) || ! is_array($playlist) ) {
		$post_meta_id = update_post_meta($playlist_id, 'playlist', [$post_id]);
		$count = 1;
	}
	else {
		$count = array_unshift($playlist, $post_id);
		$post_meta_id = update_post_meta($playlist_id, 'playlist', $playlist);
	}

	// update counter
	$counter_meta_id = update_post_meta($playlist_id, 'playlist_count', $count);

	if ( $post_meta_id && $counter_meta_id ) {
		wp_send_json_success([
			'message' => __('Playlist updated.', 'face3-nest'),
			'count' => $count,
			'id' => $playlist_id
		]);
	}

	wp_send_json_error();
}


/**
 * Remove a post id from a playlist.
 * @uses $_POST['playlist_id']
 * @uses $_POST['post_id']
 */
function remove_from_playlist() {
// security
	if ( ! check_ajax_referer() ) {
		wp_send_json_error();
	}

	// filter inputs
	$playlist_id = filter_input( INPUT_POST, 'playlist_id', FILTER_SANITIZE_NUMBER_INT );
	$post_id = filter_input( INPUT_POST, 'post_id', FILTER_SANITIZE_STRING );

	// check that playlist exists
	$playlist = get_post($playlist_id);
	if ( empty($playlist) ) {
		wp_send_json_error([
			'message' => __('Playlist not found.', 'face3-nest')
		]);
	}

	// only the owner of this playlist can edit it
	if ( get_current_user_id() != get_post_field('post_author', $playlist_id) ) {
		wp_send_json_error([
			'message' => __('You are not allowed to update this playlist. It belongs to someone else.', 'face3-nest')
		]);
	}

	// Update and save meta
	$playlist = get_post_meta($playlist_id, 'playlist', true);
	if ( ! empty($playlist) && is_array($playlist) ) {
		$new_playlist = array_diff($playlist, [$post_id]);
		$post_meta_id = update_post_meta($playlist_id, 'playlist', $playlist);

		$count = count($new_playlist);
		$counter_meta_id = update_post_meta($playlist_id, 'playlist_count', $count);

		if ( $post_meta_id && $counter_meta_id ) {
			wp_send_json_success([
				'message'   => __('Playlist updated.', 'face3-nest'),
				'id'        => $playlist_id
			]);
		}
	}

	wp_send_json_error();
}