<?php
new CMCR_Post_Likes_Per_Month_Report();

class CMCR_Post_Likes_Per_Month_Report extends CMCR_Report_Base
{
    public static $postTypes = array('influence','creator','report');

    public function init()
    {
        add_filter('cmcr_graph_tab_controls_output-' . $this->getReportSlug(), array($this, 'addGraphControls'));
        add_filter('cmcr_report_name_filter', array('CMCR_Report_Base', 'addReportNameContent'), 10, 2);
    }

    public function addGraphControls($output)
    {
        $postArray = filter_input_array(INPUT_POST);
        ob_start();
        ?>
        <form method="post" action="">
            <input type="text" name="date_from" value="<?php echo!empty($postArray['date_from']) ? $postArray['date_from'] : '' ?>" class="datepicker" />
            <input type="text" name="date_to" value="<?php echo!empty($postArray['date_to']) ? $postArray['date_to'] : '' ?>" class="datepicker" />
            <label>Show type: <?php echo $this->showTypesDropdown() ?></label>
            <input type="submit" value="Filter">
        </form>
        <?php
        $graphControlsOutput = ob_get_clean();
        $output = $graphControlsOutput . $output;
        return $output;
    }

    public function getReportSlug()
    {
        return 'post-likes-per-month';
    }

    public function getReportDescription()
    {
        return CM_Custom_Reports::__('Report displays number of post likes per month');
    }

    public function getReportName()
    {
        return CM_Custom_Reports::__('Post Likes by Month');
    }

    public function getGroups()
    {
        return array('likes' => CM_Custom_Reports::__('Likes'));
    }

    /**
     * Return the list of possible Graph Types
     * @param type $possibleGraphTypes
     * @return type
     */
    public function getPossibleGraphTypes($possibleGraphTypes)
    {
        foreach($possibleGraphTypes as $key => $value)
        {
            if( !in_array($key, array('bars', 'points', 'pie')) )
            {
                unset($possibleGraphTypes[$key]);
            }
        }
        return $possibleGraphTypes;
    }

    public function showTypesDropdown()
    {
        $postArray = filter_input_array(INPUT_POST);
        $selectedType = (isset($postArray['type']) ? $postArray['type'] : 'all');

        $types = self::$postTypes;

        ob_start();
        ?>
        <select name="type">
            <option value="all" <?php selected('all', $selectedType); ?> ><?php echo CM_Custom_Reports::__('All') ?></option>
            <?php foreach($types as $value) : ?>
                <?php
                $label = !empty($value) ? $value : CMCR_Labels::getLocalized('empty_post_type');
                ?>
                <option value="<?php echo $value; ?>" <?php selected($value, $selectedType); ?> ><?php echo ucfirst($label); ?></option>
            <?php endforeach; ?>
        </select>
        <?php
        $result = ob_get_clean();
        return $result;
    }

    public function getReportExtraOptions()
    {
        $graphOptions = array(
            'axisLabels' => array(
                'show' => true
            ),
            'xaxis'      => array(
                'axisLabel'   => 'Day',
                'mode'        => 'time',
                'timeformat'  => CM_Custom_Reports_Backend::getDateFormat('flot'),
                'minTickSize' => array(1, "day")
            ),
            'yaxis'      => array(
                'axisLabel'    => 'Likes',
                'min'          => 0,
                'minTickSize'  => 1,
                'tickDecimals' => 0
            ),
            'series'     => array(
                'bars' => array(
                    'show'     => TRUE,
                    'barWidth' => 24 * 60 * 60 * 1000,
                    'align'    => 'center'
                )
            ),
            'grid'       => array(
                'hoverable'     => TRUE,
                'clickable'     => TRUE,
                'autoHighlight' => TRUE,
            )
        );

        $reportOptions = array(
            'cron'             => TRUE,
            'graph'            => $graphOptions,
            'graph_datepicker' => array(
                'showOn'      => 'both',
                'showAnim'    => 'fadeIn',
                'dateFormat'  => CM_Custom_Reports_Backend::getDateFormat('datepicker'),
                'buttonImage' => CM_Custom_Reports_Backend::$imagesPath . 'calendar.gif',
            )
        );

        return $reportOptions;
    }

    public static function addDataFilter()
    {
        $dateQuery = array();
        $postArray = filter_input_array(INPUT_POST);

        if( !empty($postArray['date_from']) )
        {
            $dateQuery['after'] = $postArray['date_from'];
        }
        if( !empty($postArray['date_to']) )
        {
            $dateQuery['before'] = $postArray['date_to'];
        }
        else
        {
            $dateQuery['before'] = CM_Custom_Reports_Backend::getDate();
        }

        return $dateQuery;
    }

    public static function addTopFilter()
    {
        $result = NULL;
        $postArray = filter_input_array(INPUT_POST);
        if( isset($postArray['type']) )
        {
            $result = $postArray['type'];
        }
        return $result;
    }

    public function getData($dataArgs = array('json' => FALSE))
    {
        global $wpdb;

        static $savedData = array();


        $result = array();
        $viewsByDate = array();
        $viewsByType = array();

        $query_text="SELECT * FROM {$wpdb->prefix}nest_favourites where 1=1 ";


        $where =" ";

        $postArray = filter_input_array(INPUT_POST);

        if (!empty($postArray['date_from']) || !empty($postArray['date_to']) ) {
            if (!empty($postArray['date_from'])) {
                $date_from= date('Y-m-d', strtotime($postArray['date_from']));
            }
            if (!empty($postArray['date_to'])) {
                $date_to = date('Y-m-d', strtotime($postArray['date_to']. ' + 1 days'));
            } else {
                $date_to= date('Y-m-d', strtotime(CM_Custom_Reports_Backend::getDate(). ' + 1 days'));
            }
            $where.=" and created_at between '$date_from' and '$date_to'";


        }

        $postArray = filter_input_array(INPUT_POST);
        if( isset($postArray['type']) && $postArray['type']!='all' )
        {
            $post_type=$postArray['type'];
            $query_text="SELECT * FROM {$wpdb->prefix}nest_favourites , {$wpdb->prefix}posts where {$wpdb->prefix}nest_favourites.post_id = {$wpdb->prefix}posts.ID and {$wpdb->prefix}posts.post_type='$post_type' and 1=1  ";;
        }

        $query_text.=$where;

        //echo $query_text;
        $customQuery= $wpdb->prepare($query_text);
        $results = $wpdb->get_results(
            $customQuery
        );


        // $json = !empty($dataArgs['json']) ? $dataArgs['json'] : false;

        /*  if( empty($dataArgs['date_query']) )
          {
              $args['date_query'] = self::addDataFilter();
          }
          else
          {
              $args['date_query'] = $dataArgs['date_query'];
          }*/

        /*
         * Additional filter
         */
        /* if( empty($dataArgs['author']) )
         {
             $filterResult = self::addTopFilter();

             if( $filterResult !== NULL )
             {
                 $dataArgs['type'] = $filterResult;
             }
         }
         else
         {
             $dataArgs['type'] = $dataArgs['comment_type'];
         }


         if( !empty($dataArgs['author']) )
         {
             $args['user_id'] = $dataArgs['author'];
         }

         if( !empty($args['date_query']) )
         {
             $args['date_query']['inclusive'] = true;
         }

         $argsKey = sha1(maybe_serialize($args));
         if( !empty($savedData[$argsKey]) )
         {
             return $savedData[$argsKey];
         }

        */
        /*
         * Posts
         */
        //   $query = new WP_Query();
        //  $posts = $query->query($args);


        if( !empty($results) )
        {
            foreach($results as $res)
            {
                $time = strtotime($res->created_at);
                $type = get_post_type($res->post_id);

                // if(!in_array($type, self::$postTypes))
                //  {
                //     self::$postTypes = get_post_types( '', 'names' );
                //  }

                if(isset($dataArgs['type']) && $dataArgs['type'] !== NULL && $dataArgs['type'] !== 'all' && $type !== $dataArgs['type'])
                {
                    continue;
                }
                $realDate = CM_Custom_Reports_Backend::getDate($time);
                $realTime = strtotime($realDate);

                if( isset($viewsByDate[$type][$realTime]) )
                {
                    $viewsByDate[$type][$realTime] ++;
                }
                else
                {
                    $viewsByDate[$type][$realTime] = 1;
                }

                /*
                 * Sum the posts by type
                 */
                if( isset($viewsByType[$type]) )
                {
                    $viewsByType[$type] ++;
                }
                else
                {
                    $viewsByType[$type] = 1;
                }
            }

            if( !empty($viewsByDate) )
            {
                foreach($viewsByDate as $type => $viewData)
                {
                    /*
                     * No comments of given type
                     */
                    if( !array_key_exists($type, $viewsByType) )
                    {
                        continue;
                    }

                    $dataPosts = array();
                    $viewTypeName = $type;

                    if( empty($viewTypeName) )
                    {
                        $type = CMCR_Labels::getLocalized('empty_view_type');
                    }

                    ksort($viewData);

                    reset($viewData);
                    $first_key = key($viewData);
                    self::updateDataDateFrom(CM_Custom_Reports_Backend::getDate($first_key));

                    foreach($viewData as $key => $value)
                    {
                        $dataPosts[] = array((int) $key * 1000, $value);
                    }

                    $result[] = array(
                        'label' => ucfirst($type),
                        'data'  => $dataPosts
                    );
                }
            }
        }

        if( $json )
        {

            $result = json_encode($result);
        }
        $savedData[$argsKey] = $result;
        return $result;
    }

}