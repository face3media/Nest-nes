=== CM Custom WordPress Reports ===
Name: CM Custom WordPress Reports
Contributors: CreativeMindsSolutions
Donate link: https://www.cminds.com/wordpress-plugins-library/purchase-cm-custom-reports-plugin-for-wordpress/
Tags: reports, statistics, report, custom report, user report
Requires at least: 3.9
Tested up to: 4.9
Stable tag: 1.1.2

Create, display and auto-generate visual custom reports for your WordPress site. Export statistical and graphical data for an excellent monitoring tool.

== Description ==

[Product Page](https://www.cminds.com/wordpress-plugins-library/purchase-cm-custom-reports-plugin-for-wordpress/) | [Video](https://vimeo.com/121942578) | [User Guide](https://www.cminds.com/wordpress-plugins-knowledge-base-and-documentation/?hscat=306-cm-custom-reports) | [Additional CM Plugins](https://www.cminds.com/wordpress-plugins-library) 

**Create, display and auto-generate visual custom reports for your WordPress site. Export statistical and graphical data for an excellent monitoring tool.**

The Custom Reports Plugin will generate your WordPress usage reports, and track and measure different activities on your site. The custom reports data can also be exported. By using this activity reports plugin, you are adding an important tool that will help you manage content flow and usage within your WordPress site.

The custom reports plugin generates multiple reports, including product sales reports, preset reports and activity reports in the frontend of you WordPress site. This visualization plugin lets users can create multiple reports that illustrate data focused content into graphs or statistics. 

The custom reports Plugin  pro version featuresover 17 different statistical and graphical reports which makes tracking wordpress site behavior easy and user friendly. The plugin is easy to set up and use, as well as report any activity on a Wordpress site. 

[vimeo https://vimeo.com/121942578]


The pro version of the custom reports plugin includes options to export reports data to CSV and PDF, as well as schedule repeating reports. Users can filter reporting data by category, view logs of sent reports, use report templates, and customize reports labels.  

The custom reports plugin works in the Admin dashboard only. The settings of the plugin are very minimal and once installed, they can immediately display data from querying the WP site database. After several data mining activity reports, the plugin will start to accumulate statistics which can then be used to analyze different aspects of user behavior.


==  Pro Version == 

> The CM Custom Reports Plugin will Generate your WordPress usage reports, with over 17 statistic and graphic
> reports that track and measure different activities on your site. Reports can be automatically scheduled and
> sent to users by email in accordance with your settings. Reports data can also be exported to CSV or PDF. 

> Reports Included in Pro Version of the the Plugin
>
> * Comments – Reports the number of comments added
> * Comments by Type – Displays amount of comments per each post type (custom post type)
> * Comments by Author – Reports amount of comments by author
> * Trashed Pages – Reports about trashed pages
> * Pages – Report about newly published pages
> * Pages by Author – Report displays amount of pages per author
> * Trashed Posts – Report about trashed posts
> * Posts – Report about new published posts
> * Posts by Author – Report displays amount of posts by author
> * Posts by Type – Report displays amount of posts in each post type
> * Registered Users – Reports the number of registered users
> * Top Authors – Displays the top contributing authors
> * Revisions by Author – Displays amount of revisions made by each author
> * Revisions – Reports the number of revisions created
> * WP Login By User – Reports the number of user logins to your WP site by user
> * WP Login – Reports number of user logins to your WP site
> * WP Registration – Reports the number of user registrations to your WP site
>
> Pro version  Features
>
> * Export report data in Graph, CSV, PDF
> * Support Report Templates
> * Send reports on pre-defined periods to user/s by email
> * Include several graph types (Pie, Line, Points, Bars)
> * Support filtering report data by dates
> * Admin can create a list of favorite reports
> * Include log showing all sent reports
> * Ability to change basic terms and labels used in the report
> * Plugin is an infrastructure which can easily accommodate other reports

==  Keywords == 

reports, statistics, graph, user reports, information, csv,exporting,exports,pdf,reporting,report,stats,monitor,audit,activity,graph,graphs,information,track,admin,content,email,image,images,jquery,link,links,tracking,plugins,database,administration

==  Follow Us == 

[Blog](https://www.cminds.com/category/wordpress/) | [Twitter](http://twitter.com/cmplugins)  | [Google+](https://plus.google.com/u/0/+CmindsPlugins/) | [LinkedIn](https://www.linkedin.com/company/creativeminds) | [YouTube](https://www.youtube.com/user/cmindschannel) | [Pinterest](http://www.pinterest.com/cmplugins/) | [FaceBook](https://www.facebook.com/cmplugins/)

== Suggested Plugins by CreativeMinds ==

* [CM Ad Changer](https://adchanger.cminds.com/) - Manage, Track and Report Advertising Campaigns Across Sites. Can turn your Turn your WP into an Ad Server
* [CM Super ToolTip Glossary](https://glossaryplugin.com/) - Easily creates a Glossary, Encyclopaedia or Dictionary of your website's terms and shows them as a tooltip in posts and pages when hovering. With many more powerful features.
* [CM Download Manager](https://www.downloadmanagerplugin.com/) - Allows users to upload, manage, track and support documents or files in a download directory listing database for others to contribute, use and comment upon.
* [CM Answers Plugin](https://www.answersplugin.com/) - A fully-featured WordPress Questions & Answers Plugin that allows you to build multiple discussion forum systems Just like StackOverflow, Yahoo Answers and Quora, Now with MicroPayment and Anonymous posting support!.
* [CM MicroPayments](http://micropaymentplugin.com/) - Adds the in-site support for your own "virtual currency". The purpose of this plugin is to allow in-site transactions without the necessity of processing the external payments each time (quicker & easier). Developers can use it as a platform to integrate with their own plugins.
* [CM Restrict Content](http://restrictcontent.com/) - A full-featured, powerful membership solution and content restriction plugin for WordPress. Support access by role to content on your site.
* [CM OnBoarding](https://onboardingplugin.com/) - Superb Guidance tool which improves the online experience and 
the user satisfaction.
* [CM Booking Calendar](http://bookingcalendarplugin.com/) - Customers can easily schedule appointments and pay for them directly through your website.

== Installation ==

> [Detailed User Guide](https://www.cminds.com/wordpress-plugins-knowledge-base-and-documentation/?hscat=306-cm-custom-reports)


1. Upload the plugin folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Manage your CM Custom Report plugin from Left Side Admin Dashboard


== Frequently Asked Questions ==

> [More FAQ's](https://www.cminds.com/wordpress-plugins-library/purchase-cm-custom-reports-plugin-for-wordpress/#plugin-faq)


== Screenshots ==

1. Example of a report
2. Admin custom reports dashboard



== Changelog ==

> [View Release Notes in CM Custom Reports Site](https://www.cminds.com/wordpress-plugins-library/purchase-cm-custom-reports-plugin-for-wordpress/#changelog)

