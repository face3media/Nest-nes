<?php

$cminds_plugin_config = array(
	'plugin-is-pro'				 => FALSE,
	'plugin-is-addon'			 => FALSE,
	'plugin-version'			 => '1.1.1',
	'plugin-abbrev'				 => 'cmcr',
	'plugin-file'				 => CMCR_PLUGIN_FILE,
    'plugin-affiliate'               => '',
    'plugin-redirect-after-install'  => admin_url( 'admin.php?page=cm-custom-reports-settings' ),
    'plugin-show-guide'              => TRUE,
    'plugin-guide-text'              => '<div style="display:block">
        <ol>
            <li>Go to <strong>"Reports"</strong> under the CM Custom Reports  menu</li>
            <li>Choose the report you want to generate</li>
            <li>You can adjust the dates or select the report graph type.</li>
            <li>You can also download the generated report output to a PDF format</li>
        </ol>
    </div>',
    'plugin-guide-video-height'      => 240,
    'plugin-guide-videos'            => array(
        array( 'title' => 'Installation tutorial', 'video_id' => '164061174' ),
    ),
	'plugin-dir-path'			 => plugin_dir_path( CMCR_PLUGIN_FILE ),
	'plugin-dir-url'			 => plugin_dir_url( CMCR_PLUGIN_FILE ),
	'plugin-basename'			 => plugin_basename( CMCR_PLUGIN_FILE ),
	'plugin-icon'				 => '',
	'plugin-name'				 => CMCR_NAME,
	'plugin-license-name'		 => CMCR_NAME,
	'plugin-slug'				 => '',
	'plugin-short-slug'			 => 'custom-reports',
	'plugin-menu-item'			 => CMCR_SLUG_NAME,
	'plugin-textdomain'			 => CMCR_SLUG_NAME,
	'plugin-userguide-key'		 => '306-cm-custom-reports',
	'plugin-store-url'			 => 'https://www.cminds.com/wordpress-plugins-library/purchase-cm-custom-reports-plugin-for-wordpress/',
	'plugin-support-url'		 => 'https://wordpress.org/support/plugin/cm-custom-reports',
	'plugin-review-url'			 => 'https://wordpress.org/support/view/plugin-reviews/cm-custom-reports',
	'plugin-changelog-url'		 => CMCR_RELEASE_NOTES,
	'plugin-licensing-aliases'	 => array( '' ),
	'plugin-compare-table'	 => '
               <div class="suite-package" style="padding-left:10px;"><h2>The premium version of this plugin is included in CreativeMinds All plugins suite:</h2><a href="https://www.cminds.com/wordpress-plugins-library/cm-wordpress-plugins-yearly-membership/" target="_blank"><img src="'.plugin_dir_url( __FILE__ ).'CMWPPluginssuite.png"></a></div>
            <hr style="width:1000px; height:3px;">
            <div class="pricing-table" id="pricing-table"><h2 style="padding-left:10px;">Upgrade The Custom Reports Plugin:</h2>
                <ul>
                    <li class="heading" style="background-color:black;">Current Edition</li>
                    <li class="price">FREE<br /></li>
                  <li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Includes 5 Reports</li>
                    <li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Includes PDF export</li>    
                 </ul>

                <ul>
                    <li class="heading">Pro<a href="https://www.cminds.com/wordpress-plugins-library/purchase-cm-custom-reports-plugin-for-wordpress/" style="float:right;font-size:11px;color:white;" target="_blank">More</a></li>
                    <li class="price">$29.00<br /> <span style="font-size:14px;">(For one Year / Site)<br />Additional pricing options available <a href="https://www.cminds.com/wordpress-plugins-library/purchase-cm-custom-reports-plugin-for-wordpress/" target="_blank"> >>> </a></span> <br /></li>
                    <li class="action"><a href="https://www.cminds.com/downloads/cm-custom-reports/?edd_action=add_to_cart&download_id=32472&wp_referrer=https://www.cminds.com/checkout/&edd_options[price_id]=1" style="font-size:18px;" target="_blank">Upgrade Now</a></li>
                     <li style="text-align:left;"><span class="dashicons dashicons-yes"></span>All Free Version Features <span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:green" title="All free features are supported in the pro"></span></li>
<li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Includes 17 custom reports</li>
<li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Graph Type<span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:green" title="Include several graph types (Pie, Line, Points, Bars)"></span></li>
<li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Report Date Period<span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:green" title="Support filtering report data by time period"></span></li>
<li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Favorite Report<span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:green" title="Admin can create a list of favorite reports"></span></li>
<li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Reports Log<span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:green" title="Include a log showing all produced and sent reports"></span></li>
<li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Graph Export Options<span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:green" title="Graph can be exported to CSV and PDF. Using the dates filter, you can export partial data or download all info at once."></span></li>
<li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Scheduling Reports<span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:green" title="Reports can be sent on repeated dates using the report scheduling settings."></span></li>
<li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Email Templates<span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:green" title="Reports can be sent to users using several user defined templates"></span></li>
<li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Labels and Settings<span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:green" title="Plugin includes admin ability to customize graphs and reports labels according to their personal preferences as well as set their desired date format"></span></li>
<li style="text-align:left;"><span class="dashicons dashicons-yes"></span>Reports Dashboard<span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:green" title="Admin panel includes over 17 reports, so to make things easier, the Admin can create a list of favorite reports to be shown at the top part of the reports dashboard"></span></li>                 
<li class="support" style="background-color:lightgreen; text-align:left; font-size:14px;"><span class="dashicons dashicons-yes"></span> One year of expert support <span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:grey" title="You receive 365 days of a WordPress expert support. We will answer questions you have and also support any issue related to the plugin. We also provide on site support."></span><br />
                        <span class="dashicons dashicons-yes"></span> Unlimited product updates <span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:grey" title="During the year, you can update the plugin as many times as needed and receive any new release and security update"></span><br />
                        <span class="dashicons dashicons-yes"></span> Plugin can be used forever <span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:grey" title="If you choose not to renew the plugin license, you can still continue to use a long as you want."></span><br />
                        <span class="dashicons dashicons-yes"></span> Save 35% once renewing license <span class="dashicons dashicons-admin-comments cminds-package-show-tooltip" style="color:grey" title="If you choose to renew the plugin license you can do this anytime you choose. The renewal cost will be 35% off the product cost."></span></li>
                </ul>

            </div>',
);