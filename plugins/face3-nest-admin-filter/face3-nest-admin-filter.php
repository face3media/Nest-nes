<?php
/*
Plugin Name: Face3 Filter Admin Help
Plugin URI:
Description: answer to
Version: 1.0
Author:
Author URI:
*/

function nest_filter_scripts_required() {
    wp_enqueue_script( 'moment', plugin_dir_url( __FILE__ ) . 'js/moment.js', array('jquery'), '1.0' );
    wp_enqueue_script( 'bootstrapdatepicker', plugin_dir_url( __FILE__ ) . 'js/bootstrapdatepicker.js', array('jquery'), '1.0' );

    wp_enqueue_style( 'bootstrapdatepickercss', plugin_dir_url( __FILE__ ) . 'css/datepickercss.css', null, '1.0' );
    wp_enqueue_script( 'custom_date_picker_script', plugin_dir_url( __FILE__ ) . 'js/datepicker_start.js', array('jquery'), '1.0' ,true );


}
add_action('admin_enqueue_scripts', 'nest_filter_scripts_required');

add_filter('months_dropdown_results', '__return_empty_array');
add_action( 'restrict_manage_posts', 'wpse45436_admin_posts_filter_restrict_manage_posts' );
/**
 * First create the dropdown
 * make sure to change POST_TYPE to the name of your custom post type
 *
 * @author Ohad Raz
 *
 * @return void
 */
function wpse45436_admin_posts_filter_restrict_manage_posts(){

    ?>
    <input type="text" name="daterange-filter-start" placeholder="By Start Date" />

    <input type="text" name="daterange-filter" placeholder="By Expiration Date" />

    <?php $fields = get_meta_values('city',$_GET['post_type']);
    printf( '<select class="postform" name="%s">', 'city' );
    printf( '
    <option selected="selected" value="0">%s</option>', "Show All Cities" );
    foreach ( $fields as $term ) {
        if(isset($_GET["city"])){
            if($_GET["city"] == $term){
                printf( '<option selected="selected" value="%s">%s</option>', $term, $term );
            }else{
                printf( '<option value="%s">%s</option>',$term, $term );
            }
        }else{
            printf( '<option value="%s">%s</option>', $term, $term );
        }
    }
    print( '</select>' );


    $fields2 = get_meta_values('country',$_GET['post_type']);
    printf( '<select class="postform" name="%s">', 'country' );
    printf( '
    <option selected="selected" value="0">%s</option>', "Show All Countries" );
    foreach ( $fields2 as $term ) {
        if(isset($_GET["country"])){
            if($_GET["country"] == $term){
                printf( '<option selected="selected" value="%s">%s</option>', $term, $term );
            }else{
                printf( '<option value="%s">%s</option>',$term, $term );
            }
        }else{
            printf( '<option value="%s">%s</option>', $term, $term );
        }
    }
    print( '</select>' );


}


function get_meta_values( $key = '', $post_type='post', $status = 'publish' ) {

    global $wpdb;



    if( empty( $key ) )
        return;

    $r = $wpdb->get_col( $wpdb->prepare( "
        SELECT pm.meta_value FROM {$wpdb->postmeta} pm
        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
        WHERE pm.meta_key = '%s' 
        AND p.post_status = '%s' 
        AND p.post_type= '%s'
    ", $key, $status ,$post_type) );

    return $r;
}
add_filter( 'parse_query', 'wpse45436_posts_filter' );
/**
 * if submitted filter by post meta
 *
 * make sure to change META_KEY to the actual meta key
 * and POST_TYPE to the name of your custom post type
 * @author Ohad Raz
 * @param  (wp_query object) $query
 *
 * @return Void
 */
function wpse45436_posts_filter( $query ){
    global $pagenow ,$GLOBALS;



  //  var_dump( $_GET['daterange']);

    if (  is_admin() && $pagenow=='edit.php' && isset($_GET['daterange-filter']) && $_GET['daterange-filter'] != '') {

       $dateRane= explode('-',$_GET['daterange-filter']);

        $startDate= strtotime(trim($dateRane[0]));
        $endDate= strtotime(trim($dateRane[1]));



       if ($startDate && $endDate) {
           $meta_query = (array)$query->get('meta_query');

           $meta_query[] = array(
               'key' => '_expiration-date',
               'value' => array($startDate, $endDate),
               'compare' => 'BETWEEN',
               'type' => 'numeric'
           );
           $query->set('meta_query', $meta_query);
       }

    }
    if (  is_admin() && $pagenow=='edit.php' && isset($_GET['daterange-filter-start']) && $_GET['daterange-filter-start'] != '') {

        $dateRane= explode('-',$_GET['daterange-filter-start']);

        $startDate= trim($dateRane[0]);
        $endDate= trim($dateRane[1]);





        $query->set('date_query', array(
                array(
                    'after' => $startDate,
                    'before' =>$endDate,
                    'inclusive' => false
                ),
            )
        );

    }

    if (  is_admin() && $pagenow=='edit.php' && isset($_GET['city']) && $_GET['city'] != '') {
        if ($_GET['city']) {
            $meta_query = (array)$query->get('meta_query');

            $meta_query[] = array(
                'key' => 'city',
                'value' => $_GET['city'],
                'compare' => '=',
            );
            $query->set('meta_query', $meta_query);
        }

    }

    if (  is_admin() && $pagenow=='edit.php' && isset($_GET['country']) && $_GET['country'] != '') {
        if ($_GET['country']) {
            $meta_query = (array)$query->get('meta_query');

            $meta_query[] = array(
                'key' => 'country',
                'value' => $_GET['country'],
                'compare' => '=',
            );
            $query->set('meta_query', $meta_query);
        }

    }

}