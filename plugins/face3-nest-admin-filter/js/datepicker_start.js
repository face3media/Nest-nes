jQuery('input[name="daterange-filter"]').daterangepicker({
    autoUpdateInput: false,
    locale: {
        cancelLabel: 'Clear'
    }
});


jQuery('input[name="daterange-filter"]').on('apply.daterangepicker', function(ev, picker) {
    jQuery(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
});

jQuery('input[name="daterange-filter"]').on('cancel.daterangepicker', function(ev, picker) {
    jQuery(this).val('');
});

if (findGetParameter('daterange-filter'))
{
    var param=findGetParameter('daterange-filter');
    if (param!=null && param!= 'undefined' && param !='') {
        jQuery('input[name="daterange-filter"]').val(findGetParameter('daterange-filter').replace(/\+/g, ''));
    }

}


jQuery('input[name="daterange-filter-start"]').daterangepicker({
    autoUpdateInput: false,
    locale: {
        cancelLabel: 'Clear'
    }
});


jQuery('input[name="daterange-filter-start"]').on('apply.daterangepicker', function(ev, picker) {
    jQuery(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
});

jQuery('input[name="daterange-filter-start"]').on('cancel.daterangepicker', function(ev, picker) {
    jQuery(this).val('');
});

if (findGetParameter('daterange-filter-start'))
{
    var param=findGetParameter('daterange-filter-start');
    if (param!=null && param!= 'undefined' && param !='') {
        jQuery('input[name="daterange-filter-start"]').val(findGetParameter('daterange-filter-start').replace(/\+/g, ''));
    }

}



function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}