<?php

/*
Plugin Name: Nest - Filters & Favorites
Description: Allow post filtering and marking a post as "favourite"
Version: 1.0
Author: Face3
Author URI: http://face3.com
License: GPL2
Domain: face3-nest
*/

namespace Face3\Nest\Filters;

defined('ABSPATH') || exit;

define('FACE3_NEST_FILTERS_PLUGIN_FILE', __FILE__);
define('FACE3_NEST_FILTERS_FAVOURITE_TABLE_NAME', 'nest_favourites');
define('FACE3_NEST_FILTERS_PAGEVIEWS_TABLE_NAME', 'nest_pageviews');
define('FACE3_NEST_FILTERS_PAGEVIEWS_COUNT_META_KEY', 'nest_view_count');
define('FACE3_NEST_FILTERS_PAGEVIEWS_LATEST_META_KEY', 'nest_latest_page_viewed');
define('FACE3_NEST_FILTERS_PAGEVIEWS_LATEST_LIMIT', 10);
define('FACE3_NEST_FILTERS_FAVOURITE_COUNT_META_KEY', 'nest_like_count');
define('FACE3_NEST_FILTERS_FAVOURITE_USER_META_KEY', 'nest_likes');
define('FACE3_NEST_FILTERS_AJAX_ACTION', 'nest_ajax_action');

require_once 'src/database/database.php';
\register_activation_hook(__FILE__,'Face3\Nest\Filters\database\install');

include_once 'src/frontend/ajax-functions.php';
add_action( 'wp_ajax_like', 'Face3\Nest\Filters\frontend\like');
add_action( 'wp_ajax_unlike', 'Face3\Nest\Filters\frontend\unlike');
add_action( 'wp_ajax_pageview', 'Face3\Nest\Filters\frontend\page_view');
add_action( 'output_ajax_pageview', 'Face3\Nest\Filters\frontend\output_ajax_pageview');
add_action( 'wp_enqueue_scripts', 'Face3\Nest\Filters\frontend\output_ajax_config', 20);

include_once 'src/frontend/shortcode.php';
add_shortcode('recently_viewed', 'Face3\Nest\Filters\frontend\recently_viewed');
add_shortcode('add_to_playlist', 'Face3\Nest\Filters\frontend\add_to_playlist');
add_shortcode( 'share_with_user', 'Face3\Nest\Filters\frontend\share_with_user');

include_once 'src/frontend/query_filters.php';
add_filter('nest_query_filters', 'Face3\Nest\Filters\frontend\query_post_filter');
