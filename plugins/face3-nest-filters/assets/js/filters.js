;(function($) {
	var request;
	$(document).ready( function () {
		$('body').on('click', '.fa-heart-o', function() {
			var $this = $(this),
				post_id = $this.data('post-id');

			if ( post_id ) {
				$this.removeClass('fa-heart-o').addClass('fa-spinner fa-pulse');
				request = $.post(nest_filters_config.ajax_url, {
					'_wpnonce' : nest_filters_config.nonce,
					'action' : 'like',
					'post_id' : post_id
				}, function(resp) {
					$this.removeClass('fa-spinner fa-pulse').addClass('fa-heart');
					$this[0].nextSibling.data = " " + resp.data.count;
				});
			}
		})
		.on('click', '.fa-heart', function() {
			var $this = $(this),
				post_id = $this.data('post-id');

			if ( post_id ) {
				$this.removeClass('fa-heart').addClass('fa-spinner fa-pulse');
				request = $.post(nest_filters_config.ajax_url, {
					'_wpnonce' : nest_filters_config.nonce,
					'action' : 'unlike',
					'post_id' : post_id
				}, function(resp) {
					$this.removeClass('fa-spinner fa-pulse').addClass('fa-heart-o');
					$this[0].nextSibling.data = " " + resp.data.count;
				});
			}
		})
		/* Add to playlist functions */
		.on('click', '.fa-plus-circle', function() {
			var $this = $(this);

			$this.next().slideToggle('fast');
		})
		.on('click', '.add-to-playlist-show-create-new', function() {
			var $this = $(this),
                $playlistSelect = $this.parent().find('select'),
                $playlistNewName = $this.parent().find('input');

			if ( $playlistSelect.is(':visible') ) {
                $this.parent().find('select').hide();
                $this.parent().find('input').show();
                $this.text('Cancel');
			}
			else {
                $this.parent().find('input').hide();
                $this.parent().find('select').show();
                $this.text('Create new list');
			}
		})
		.on('click', '.add-to-playlist-save', function() {
			var $this = $(this),
				$playlistSelect = $this.parent().find('select'),
				$playlistNewName = $this.parent().find('input'),
				$container	= $this.parents('.add-to-playlist-container'),
				$containerPlaylist = $container.find('.add-to-playlist-select-container'),
				post_id = $container.data('post-id');

			if ( $playlistSelect.is(':visible') ) {
				// save selection to server
                $container.find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-spinner fa-pulse');
				$containerPlaylist.hide();
                $.post(nest_filters_config.ajax_url, {
                    '_wpnonce' : nest_filters_config.nonce,
                    'action' : 'add_to_playlist',
					'playlist_id': $playlistSelect.val(),
                    'post_id' : post_id
                }, function(resp) {
                    $container.find('.fa-spinner').removeClass('fa-spinner fa-pulse').addClass('fa-plus-circle');
                });
			}
			else {
                // create new playlist first, then add post to playlist
                $container.find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-spinner fa-pulse');
                $containerPlaylist.hide();
                $.post(nest_filters_config.ajax_url, {
                    '_wpnonce' : nest_filters_config.nonce,
                    'action' : 'create_playlist',
                    'playlist_name': $playlistNewName.val()
                }, function(resp) {
                	if ( resp.success ) {
                        $.post(nest_filters_config.ajax_url, {
                            '_wpnonce': nest_filters_config.nonce,
                            'action': 'add_to_playlist',
                            'playlist_id': resp.data.id,
                            'post_id': post_id
                        }, function (resp) {
                            $container.find('.fa-spinner').removeClass('fa-spinner fa-pulse').addClass('fa-plus-circle');
                        });
                    }
                });
			}

		})
        /* Add to playlist functions */
		.on('click', '.fa-upload', function() {
			var $this = $(this);

			$this.next().slideToggle('fast');
		})
		.on('click', '.share-to-user-send', function() {
			var $this = $(this),
				$parentContainer = $this.parents('.share-to-user-container'),
				$users = $parentContainer.find('input'),
				post_id = $parentContainer.data('post-id');

			// share to users via ajax
            $parentContainer.find('.fa-upload').removeClass('fa-upload').addClass('fa-spinner fa-pulse')
				.next().hide();

			$.post(nest_filters_config.ajax_url, {
				'_wpnonce': nest_filters_config.nonce,
				'action': 'share_post',
				'share_with_user_email': $users.val(),
				'post_id': post_id
			}, function (resp) {
                $parentContainer.find('.fa-spinner').removeClass('fa-spinner fa-pulse').addClass('fa-upload');
			});

		})
	});
})(jQuery);
