<?php
namespace Face3\Nest\Filters\database;

/**
 * Add favourite to database
 * @param $user_id
 * @param $post_id
 *
 * @return int
 */
function favourite($user_id, $post_id)
{
	global $wpdb;
	$table_name = $wpdb->prefix . FACE3_NEST_FILTERS_FAVOURITE_TABLE_NAME;

	$inserted = $wpdb->replace($table_name, [
		'user_id' => $user_id,
		'post_id' => $post_id
	], [ '%d', '%d' ] );

	if ( $inserted === false ) {
		error_log( var_export($inserted, true) );

	}

	return update_favourite_meta_count($post_id);
}

/**
 * Remove favourite from database
 * @param $user_id
 * @param $post_id
 *
 * @return int
 */
function unfavourite($user_id, $post_id)
{
	global $wpdb;
	$table_name = $wpdb->prefix . FACE3_NEST_FILTERS_FAVOURITE_TABLE_NAME;

	$deleted = $wpdb->delete($table_name, [ 'user_id' => $user_id, 'post_id' => $post_id ], [ '%d', '%d' ] );

	if ( $deleted == false ) {
		error_log( var_export($deleted, true) );

	}

	return update_favourite_meta_count($post_id);
}

/**
 * Update count stored in post meta for easier management
 * @param $post_id
 * @return int
 */
function update_favourite_meta_count($post_id) {
	global $wpdb;
	$favourite_table = $wpdb->prefix . FACE3_NEST_FILTERS_FAVOURITE_TABLE_NAME;

	$countSQL =<<<SQL
SELECT COUNT(id)
   FROM $favourite_table
   WHERE post_id = %d
SQL;
	$likeCount = intval( $wpdb->get_var( $wpdb->prepare( $countSQL, [ $post_id ] ) ) );

	update_post_meta($post_id, FACE3_NEST_FILTERS_FAVOURITE_COUNT_META_KEY, $likeCount);

	return $likeCount;
}


/**
 * Add page view to database
 * @param $user_id
 * @param $post_id
 *
 * @return false|int
 */
function page_view($user_id, $post_id)
{
	global $wpdb;
	$table_name = $wpdb->prefix . FACE3_NEST_FILTERS_PAGEVIEWS_TABLE_NAME;

	$inserted = $wpdb->insert($table_name, [ 'user_id' => $user_id, 'post_id' => $post_id ], [ '%d', '%d' ] );
	if ( $inserted ) {
		update_pageview_meta_count($post_id);
		update_latest_page_viewed($user_id, $post_id);
	}

	return $inserted;
}

/**
 * Remove page view from database
 * @TODO do we need this function?
 * @param $user_id
 * @param $post_id
 *
 * @return false|int
 */
function page_unview($user_id, $post_id)
{
	global $wpdb;
	$table_name = $wpdb->prefix . FACE3_NEST_FILTERS_PAGEVIEWS_TABLE_NAME;

	$deleted = $wpdb->delete($table_name, [ 'user_id' => $user_id, 'post_id' => $post_id ], [ '%d', '%d' ] );

	if ( $deleted ) {
		update_pageview_meta_count($post_id);
	}

	return $deleted;
}

/**
 * Update count stored in post meta for easier management
 * @param $post_id
 */
function update_pageview_meta_count($post_id) {
	global $wpdb;
	$pageviews_table = $wpdb->prefix . FACE3_NEST_FILTERS_PAGEVIEWS_TABLE_NAME;

	$count = intval( $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(id) FROM $pageviews_table WHERE post_id = %d", $post_id) ) );

	update_post_meta($post_id, FACE3_NEST_FILTERS_PAGEVIEWS_COUNT_META_KEY, $count);
}

/**
 * Update user meta with latest page viewed. Limit number that is saved.
 * @param $user_id
 * @param $post_id
 * @return bool success
 */
function update_latest_page_viewed($user_id, $post_id) {
	$latest = get_user_meta($user_id, FACE3_NEST_FILTERS_PAGEVIEWS_LATEST_META_KEY, true);
	if ( empty($latest) || ! is_array ($latest) ) {
		// init or reset meta
		$latest = [];
	}

	// remove existing
	if ( is_array($latest) && ($key = array_search($post_id, $latest)) !== false ) {
		unset($latest[$key]);
	}

	// put latest page first and keep only a limited amount
	if ( array_unshift($latest, $post_id) > FACE3_NEST_FILTERS_PAGEVIEWS_LATEST_LIMIT ) {
		$latest = array_slice($latest, 0, FACE3_NEST_FILTERS_PAGEVIEWS_LATEST_LIMIT);
	}

	return (bool)update_user_meta($user_id, FACE3_NEST_FILTERS_PAGEVIEWS_LATEST_META_KEY, $latest);
}

/**
 * Create database to store favourites
 */
function install() {
	global $wpdb;

	$favourite_table_name = $wpdb->prefix . FACE3_NEST_FILTERS_FAVOURITE_TABLE_NAME;
	$pageviews_table_name = $wpdb->prefix . FACE3_NEST_FILTERS_PAGEVIEWS_TABLE_NAME;

	$charset_collate = $wpdb->get_charset_collate();

	// install favourite table
	if($wpdb->get_var("SHOW TABLES LIKE '$favourite_table_name'") != $favourite_table_name) {
		$sql = <<<SQL
CREATE TABLE $favourite_table_name (
  id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  post_id bigint(20) unsigned NOT NULL,
  user_id bigint(20) unsigned NOT NULL,
  created_at datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE favourite (post_id, user_id),
  KEY created_at (created_at)
) $charset_collate;
SQL;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}

	// install pageviews table
	if($wpdb->get_var("SHOW TABLES LIKE '$pageviews_table_name'") != $pageviews_table_name) {
		$sql = <<<SQL
CREATE TABLE $pageviews_table_name (
  id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  post_id bigint(20) unsigned NOT NULL,
  user_id bigint(20) unsigned NOT NULL,
  created_at datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY  (id),
  KEY created_at (created_at)
) $charset_collate;
SQL;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
}
