<?php
namespace Face3\Nest\Filters\frontend;

use function Face3\Nest\Filters\database\favourite as insert_favourite;
use function Face3\Nest\Filters\database\unfavourite as delete_favourite;
use function Face3\Nest\Filters\database\page_view as record_page_view;

/**
 * Create a new like entry for this user
 * @uses $_POST['post_id']
 */
function like()
{
	// security
	if ( ! check_ajax_referer(FACE3_NEST_FILTERS_AJAX_ACTION) ) {
		wp_send_json_error();
	}

	// filter inputs
	$user_id = get_current_user_id();
	$post_id = filter_input( INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT );

	$count = insert_favourite( $user_id, $post_id );

    wp_send_json_success( [
        'id'      => $post_id,
        'message' => __( 'Post liked', 'face3-nest' ),
        'count' => $count
    ] );
}


/**
 * Remove like entry for this user
 * @uses $_POST['post_id']
 */
function unlike()
{
	// security
	if ( ! check_ajax_referer(FACE3_NEST_FILTERS_AJAX_ACTION) ) {
		wp_send_json_error();
	}

	// filter inputs
	$user_id = get_current_user_id();
	$post_id = filter_input( INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT );

	$count = delete_favourite($user_id, $post_id);

    wp_send_json_success([
        'id' => $post_id,
        'message' => __('Post unliked', 'face3-nest'),
        'count' => $count
    ]);
}

/**
 * Record a view on a post, keep it in session as well
 */
function page_view() {
	// security
	if ( ! check_ajax_referer(FACE3_NEST_FILTERS_AJAX_ACTION) ) {
		wp_send_json_error();
	}

	// filter inputs
	$user_id = get_current_user_id();
	$post_id = filter_input( INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT );

	if ( record_page_view($user_id, $post_id) ) {
		wp_send_json_success([
			'id' => $post_id,
			'message' => __('Page view recorded.', 'face3-nest'),
		]);
	}

	wp_send_json_error([
		'message' => __('Could not record this page view.', 'face3-nest'),
	]);
}

function output_ajax_pageview($post_id) {
	$ajax_nounce = wp_create_nonce(FACE3_NEST_FILTERS_AJAX_ACTION);
	?>
<script>
	(function($,document) {
		if (typeof $ !== 'undefined') {
			$(document).ready(function() {
				$.post( <?= wp_json_encode( admin_url( 'admin-ajax.php', 'relative' ) ); ?>, {
					action: "pageview",
					_wpnonce: <?= wp_json_encode( $ajax_nounce ); ?>,
					post_id: <?= wp_json_encode( $post_id ); ?>

				} );
			});
		}
	})(jQuery, document);
</script>
<?php
}

/**
 * Output js config object
 */
function output_ajax_config() {
	wp_enqueue_script('nest-filters-js', plugins_url('assets/js/filters.js', FACE3_NEST_FILTERS_PLUGIN_FILE ), ['jquery']);
	$config = [
		'nonce' => wp_create_nonce(FACE3_NEST_FILTERS_AJAX_ACTION),
		'ajax_url' => admin_url('admin-ajax.php')
	];

	wp_localize_script('nest-filters-js', 'nest_filters_config', $config);
}
