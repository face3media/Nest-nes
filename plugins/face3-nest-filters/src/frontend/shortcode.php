<?php
namespace Face3\Nest\Filters\frontend;

/**
 * View a list of recently viewed posts by the user
 * @return mixed
 */
function recently_viewed() {
	ob_start();
	$latest_viewed = get_user_meta(get_current_user_id(), FACE3_NEST_FILTERS_PAGEVIEWS_LATEST_META_KEY, true);

	if ( ! empty($latest_viewed) ) :
		$posts = new \WP_Query( [
			'post__in' => $latest_viewed,
			'posts_per_page' => -1,
			'orderby' => 'post__in',
			'post_type' => ['creator','influence','report']
		] );

		if ( $posts->have_posts() ) :
		?>
		<ul class="grid">
		<?php
			while ( $posts->have_posts() ) : $posts->the_post(); ?>
			<li class="col col-1-6" style="background: url('<?php echo get_the_post_thumbnail_url()?>') no-repeat center center">
				<div class="name"><?php the_title(); ?></div>
				<div class="likes">
					<i data-post-id="<?php the_ID(); ?>" class="fa <?= is_liked() ? 'fa-heart' : 'fa-heart-o'; ?>" aria-hidden="true"></i>&nbsp;<?= intval(get_post_meta( get_the_ID(), FACE3_NEST_FILTERS_FAVOURITE_COUNT_META_KEY, true)); ?>
					<!-- <i class="fa fa-heart" aria-hidden="true"></i> -->
				</div>
				<div class="category">
					<?php
					$icon=null;
					switch (get_post_type()) {
						case 'creator':
							$icon='ico-creator.svg';
							break;
						case 'influence':
							$icon='ico-influence.svg';
							break;
						case 'report':
							$icon='ico-report.svg';
							break;
					}  ?>
					<img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $icon ?>" alt="<?php echo ucfirst(get_post_type())?>" />
                </div>
			</li>
			<?php
			endwhile;
			wp_reset_postdata(); ?>
		</ul>
	<?php
		endif; // have posts
	endif; // user meta not empty
	return ob_get_clean();
}

/**
 * Create a UI widget for adding a post to a playlist
 * @param array $shortcode_options
 * @return string
 */
function add_to_playlist($shortcode_options) {
	ob_start();
	static $shortcode_id = 0;
	$shortcode_id++;

	$options = shortcode_atts([
        'post_id' => get_the_ID(),
    ], $shortcode_options);

	$post_args = [
        'post_type' => 'playlist',
        'author' => get_current_user_id(),
        'no_found_rows' => true,
        'posts_per_page' => -1
    ];
	$user_playlists = new \WP_Query($post_args);
	?>
    <div style="display:none;" data-post-id="<?=$options['post_id']?>" class="add-to-playlist-container">
        <div class="add-to-playlist-select-container">
            <button title="Close (Esc)" type="button" class="mfp-close">×</button>
        <?php
        if ( $user_playlists->have_posts() ) :
            ?>
            
            <!--
            <select title="Select playlist" name="add_to_playlist<?=$shortcode_id?>">
            <?php
            while ( $user_playlists->have_posts() ) : $user_playlists->the_post(); ?>
                <option value="<?php the_ID(); ?>"><?= strip_tags(get_the_title())?></option>
            <?php
            endwhile;
            wp_reset_postdata(); ?>
            </select>
            -->
        <?php
        endif; // have posts ?>
           
            <input placeholder="New list name...">
           
            <a class="add-to-playlist-show-create-new">
                <i class="fa fa-plus ease" aria-hidden="true"></i>
                <?= __('Create new list', 'face3-nest')?>
            </a>
            
            <a class="add-to-playlist-save">
                <i class="fa fa-floppy-o ease" aria-hidden="true"></i>
                <?=__('Save', 'face3-nest');?>
            </a>
            
            <ul>
            <?php while ( $user_playlists->have_posts() ) : $user_playlists->the_post(); ?>
                <li class="ease" value="<?php the_ID(); ?>"><?= strip_tags(get_the_title())?></li>
                <?php endwhile; wp_reset_postdata(); ?>
            </ul>
            
        </div>
    </div>
    <?php
	return ob_get_clean();
}


/**
 * Create a UI widget for sharing a post to a user
 * @param array $shortcode_options
 * @return string
 */
function share_with_user($shortcode_options) {
    wp_enqueue_script('nest-users');
    wp_enqueue_script('nest-user-suggest');
	ob_start();
	static $shortcode_id = 0;
	$shortcode_id++;

	$options = shortcode_atts([
		'post_id' => get_the_ID(),
	], $shortcode_options);


	?>
    <div style="display:none;" data-post-id="<?=$options['post_id']?>" class="share-to-user-container">
        <div class="share-to-user-select-container">
            <button title="Close (Esc)" type="button" class="mfp-close">×</button>
            <form>
                <div class="ui-widget">
                    <input placeholder="Search for email..." class="nest-suggest-user" name="shareemail" data-autocomplete-field="user_email">
                </div>
            </form>

            <a class="share-to-user-send"><?=__('Send', 'face3-nest');?></a>
        </div>
    </div>
	<?php
	return ob_get_clean();
}