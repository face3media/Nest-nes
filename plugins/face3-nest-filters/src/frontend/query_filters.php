<?php
namespace Face3\Nest\Filters\frontend;

/**
 * @param $args array of arguments for WP_Query
 * @return array
 */
function query_post_filter($args) {
	if ( filter_has_var(INPUT_GET, 'filter') ) {
		$filter = strtolower(filter_input( INPUT_GET, 'filter', FILTER_SANITIZE_STRING ));

		if ( ! empty($filter) && in_array($filter, ['featured','recent','liked','commented']) ) {
			switch ($filter) {
				case 'featured':
					$args['meta_key'] = 'featured';
					$args['orderby'] = 'meta_value_num';
					$args['order'] = 'DESC';
					break;
				case 'liked':
					$args['meta_key'] = FACE3_NEST_FILTERS_FAVOURITE_COUNT_META_KEY;
					$args['orderby'] = 'meta_value_num';
					$args['order'] = 'DESC';
					break;
				case 'commented':
					$args['orderby'] = 'comment_count';
					$args['order'] = 'DESC';
					break;
				case 'recent':
				default:
					$args['orderby'] = 'date';
					$args['order'] = 'DESC';
					break;
			}
		}
	}

	return $args;
}

/**
 * Did the current user like this post?
 * @param null|int $post_id
 * @return bool
 */
function is_liked( $post_id = null ) {
	global $wpdb;
	$favourite_table_name = $wpdb->prefix . FACE3_NEST_FILTERS_FAVOURITE_TABLE_NAME;
	$user_id = get_current_user_id();
	$post_id = $post_id ?: get_the_ID();

	$sql =<<<MYSQL
SELECT EXISTS(SELECT * from $favourite_table_name where post_id = %d and user_id = %d)
MYSQL;

	return (bool)$wpdb->get_var($wpdb->prepare($sql, [$post_id, $user_id]));
}