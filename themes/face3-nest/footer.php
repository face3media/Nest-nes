<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Face3_Nest
 */

?>

	</div><!-- #content -->

</div><!-- #page -->

<div class="footer">
    <div class="footer-menu">
    <ul class="categories">
        <li><a class="active" href="<?php echo  home_url()?>">All</a>&nbsp;&nbsp;/&nbsp;</li>
        <li><a href="<?php echo get_post_type_archive_link('creator')?>">Creators</a>&nbsp;&nbsp;/&nbsp;</li>
        <li><a href="<?php echo get_post_type_archive_link('influence')?>">Influences</a>&nbsp;&nbsp;/&nbsp;</li>
        <li><a href="<?php echo get_post_type_archive_link('report')?>">Reports</a></li>
    </ul>
    </div>
</div>

<script>
jQuery(document).ready(function() {
 var contentHeight = jQuery(window).height();
 var footerHeight = jQuery('.footer').height();
 var footerTop = jQuery('.footer').position().top + footerHeight;
 if (footerTop < contentHeight) {
     jQuery('.footer').css('margin-top', 10+ (contentHeight - footerTop) + 'px');
   }
 });
</script>

<?php wp_footer(); ?>

</body>

</html>
