<?php
/**
 * Template Name: All Posts
 *
 * A custom page template for displaying all posts.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

    <div id="container">
        <div id="content" role="main">

            <?php
            $currentPage = (get_query_var('page')) ? get_query_var('page') : 1;
            $batchSize = 18;
            //4 Total batches on the page. 1 added with the loop, 3 via load more plugin
            $batchesPerPage = 3;
            $paged = (($currentPage-1) * $batchesPerPage)+1;
            $args = array('posts_per_page' => $batchSize, 'paged' => $paged,'post_type'=>array('creator','influence','report') ,'post_status'=>'publish');
            $args = apply_filters('nest_query_filters', $args);
            query_posts($args); ?>
            <!-- the loop -->
            <ul class="grid">
                <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
                
                <li class="col col-1-6" style="background: url('<?php echo get_the_post_thumbnail_url()?>') no-repeat center center">
                    <div class="name"><?php the_title(); ?></div>
                    <div class="likes">
                        <i data-post-id="<?= get_the_ID();?>" class="fa <?= \Face3\Nest\Filters\frontend\is_liked() ? 'fa-heart' : 'fa-heart-o'?>" aria-hidden="true"></i>&nbsp;<?= intval(get_post_meta(get_the_ID(), FACE3_NEST_FILTERS_FAVOURITE_COUNT_META_KEY, true))?>
                        <!-- <i class="fa fa-heart" aria-hidden="true"></i> -->
                    </div>
                    <div class="category">
                        <?php
                        $icon=null;
                        switch (get_post_type()) {
                            case 'creator':
                                $icon='ico-creator.svg';
                                break;
                            case 'influence':
                                $icon='ico-influence.svg';
                                break;
                            case 'report':
                                $icon='ico-report.svg';
                                break;
                        }  ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $icon ?>" alt="<?php echo ucfirst(get_post_type())?>" />
                        
                    </div>
                    <a href="<?php the_permalink(); ?>"></a>
                </li>

                <?php endwhile; ?>
                <?php echo do_shortcode('[ajax_load_more repeater="default"  post_type="creator,influence,report" offset="'. ($paged * $batchSize) .'"  posts_per_page="'. $batchSize .'" destroy_after="'. ($batchesPerPage-1) .'" pause="false"]');?>

            </ul>

            <!-- pagination -->
            <div id="pagination" class="clearfix">
                <?php
                    //Need to replace the page number since it is not correct due to loading some data from ajax
                    $nextPostsLink = get_next_posts_link('Next Page');
                    $prevPostsLink = get_previous_posts_link('Previous Page');
                ?>
                <div class="page_next"><?php echo str_replace($paged+1, $currentPage+1, $nextPostsLink); ?></div>
                <div class="page_prev"><?php echo str_replace($paged-1, $currentPage-1, $prevPostsLink); ?></div>
            </div>

        <?php else : ?>
                <!-- No posts found -->
            <?php endif; ?>


        </div><!-- #content -->
    </div><!-- #container -->

<?php get_footer(); ?>