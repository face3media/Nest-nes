<?php $gallery = get_field('gallery'); ?>

    <?php

    if( $gallery || get_the_post_thumbnail_url(get_the_ID(),'full') !=null ): ?>
        <?php $featuredImageId = get_post_thumbnail_id(); ?>
        <ul>
            <li class="featured" style="background: url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'full') ?>');">
                <a href="<?php echo get_the_post_thumbnail_url(get_the_ID(),'full')?>"></a>
            </li>

            <?php foreach( $gallery as $image):   ?>
                <?php
                    if ($featuredImageId == $image['ID']) {
                        continue;
                    }
                ?>

                <?php if ($image['type']=='video') :
                    ?>

                    <li class='video-link-parent' style="background: url('<?php echo rt_media_get_video_thumbnail($image['ID'])?>');" >
                        <a  class="video-link" href = "#my-video<?php echo $image['ID']?>" >  <div class="fa fa-play play-icon" aria-hidden="true"></div>
                        </a>
                        <div id="my-video<?php echo $image['ID']?>" class="mfp-hide video-popup ">
                            <video  class="video-js  vjs-default-skin vjs-big-play-centered" controls preload="auto" width="640" height="264"
                                    poster="<?php echo rt_media_get_video_thumbnail($image['ID']); ?>" data-setup='{"aspectRatio" :"16:9"}'>
                                <source src="<?php echo  rtt_get_media_url($image['ID']); ?>" type='video/mp4'>
                                <p class="vjs-no-js">
                                    To view this video please enable JavaScript, and consider upgrading to a web browser that
                                </p>
                            </video>
                        </div>
                    </li >



                <?php  else:
                    ?>
                    <li style = "background: url('<?php echo $image['sizes']['large']; ?>');" >
                        <a href = "<?php echo $image['url']; ?>" ></a >
                    </li >

                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>