<?php
/* Template Name: Login Template */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Face3_Nest
 *
 *
 */

if ( is_user_logged_in() ) {
    wp_redirect( home_url() ); exit;

}
global $redux_builder_nest;
$logo = $redux_builder_nest['nest-logo-white']['url'];

add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
    $background_image= get_field('background_image');

    if(!empty($background_image))
    {
        $classes[]='full_background';
    }
    return $classes;
}


get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post();


			?>
            <div class="panel-container">
			<div class="login-logo">		<?php	the_custom_logo(); ?>
            </div>
                <?php
			the_content();
               ?>
            </div>
                 <?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
$background_image= get_field('background_image');

if(!empty($background_image))
{
    ?>
    <style>
.full_background
    {
    background-size: cover;
    background-image: url('<?php echo $background_image['url']?>');
    }

    </style>
<?
}

?>

</div>


</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
