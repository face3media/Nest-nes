<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Face3_Nest
 */

$creators_objects = get_field('collaborators');

$gallery = get_field('gallery');
$profile_image = get_field('profile_image');


$profile_image_medium = $profile_image['sizes'][ 'medium' ];

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <?php
            while ( have_posts() ) : the_post();
                do_action('output_ajax_pageview', get_the_ID());
                ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <div class="entry-content">

                    <div class="details">

                        <div class="title">
                            <div class="category">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/ico-creator.svg" alt="<?php echo ucfirst(get_post_type())?>" />
                            </div>

                            <?php the_title( '<h1>', '</h1>' ); ?>
                        </div>

                        <div class="profile">
                            <div class="col-1-2">
                                <div class="thumbnail" style="background: url('<?php echo $profile_image_medium?>') no-repeat;"></div>
                            </div>
                            <div class="contact col-1-2">
                                <strong>Various</strong>
                                <ul>
                                    <li><?php if( $city = get_field('city')) echo $city ;?><?php if( $country = get_field('country')) echo ', '.$country ;?></li>
                                    <li><a href="<?php if( $website = get_field('website')) echo $website ;?>" target="_blank"><?php if( $website = get_field('website')) echo $website ;?></a></li>
                                    <li><a href="mailto:<?php if( $email = get_field('email')) echo $email ;?>" target="_blank"><?php if( $email = get_field('email')) echo $email ;?></a></li>
                                    <li><?php if( $phone_number = get_field('phone_number')) echo $phone_number ;?></li>
                                </ul>
                                <ul class="icons">
                                    <li><i data-post-id="<?= get_the_ID();?>" class="fa <?= \Face3\Nest\Filters\frontend\is_liked() ? 'fa-heart' : 'fa-heart-o'?>" aria-hidden="true"></i>&nbsp;<?= intval(get_post_meta(get_the_ID(), FACE3_NEST_FILTERS_FAVOURITE_COUNT_META_KEY, true))?></li>
                                    <li><?=do_shortcode('[add_to_playlist]')?></li>
                                    <li><i class="fa fa-upload" aria-hidden="true"></i></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="other">
                           <?php $posttags = get_the_tags();?>
                             <?php if( $posttags ): ?>

                            <div class="keywords">
                                <strong>Keywords</strong>
                                <ul>


                                <?php foreach (get_the_tags() as $tag):?>
                                     <li>
                                           <a href="<?php  get_tag_link($tag->term_id) ?>">
                                                       <?php echo $tag->name; ?>
                                         </a>
                                      </li>
                                <?php endforeach; ?>
                                </ul>
                            </div>
                             <?php endif; ?>

                            <?php $reports=  get_reports_for_creator(get_the_id()); ?>
                            <?php $collaborators=  get_collaborators_for_creator(get_the_id());  ?>
                            <?php $influences=  get_influences_for_creator(get_the_id()); ?>


                            <?php if( $reports || $collaborators ): ?>
                                <div class="mentioned">
                                    <strong>Mentioned in</strong>
                                    <ul>
                                        <?php foreach( $reports as $report):?>


                                            <li>
                                                <a href="<?php the_permalink($report); ?>">
                                                    <?php echo get_the_title($report); ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                        <?php if( $collaborators ): ?>

                                            <?php foreach( $collaborators as $creator):  ?>

                                                <li>
                                                    <a href="<?php the_permalink($creator); ?>">
                                                        <?php  echo get_the_title($creator); ?>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>

                                        <?php endif;?>
                                        <?php if( $influences ): ?>

                                                <?php foreach( $influences as $influence):  ?>

                                                    <li>
                                                        <a href="<?php the_permalink($influence); ?>">
                                                            <?php echo get_the_title($influence)?>
                                                        </a>
                                                    </li>
                                                <?php endforeach; ?>

                                        <?php endif; ?>
                                    </ul>

                                </div>
                            <?php endif; ?>
                        </div>
                       


                        <div class="collaborators clearfix section">
                            <?php if( $creators_objects ): ?>
                                <div class="title">Collaborators</div>
                                <ul>
                                    <?php foreach( $creators_objects as $creator):  ?>

                                        <li>
                                            <a href="<?php the_permalink($creator->ID); ?>">
                                                <div class="thumbnail" style="background: url('<?php echo get_field('profile_image',$creator->ID)['sizes'][ 'medium' ]?>') no-repeat center center"></div>
                                                <?php echo $creator->post_title ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>

                            <div class="clearfix"></div>
                        </div>

                        <?php
                            if ( comments_open() || get_comments_number() ) :
                            comments_template();
                            endif;
                        ?>

                    </div>
                    
                    <div class="gallery">
                        <?php get_template_part( 'template-parts/content', 'gallery' ); ?>

                        <div class="related clearfix section">
                            <?php
                            $related_posts = get_related_posts_by_tag(get_the_id(),3);

                            if( $related_posts ):
                                ?>
                                <div class="title">Suggested Posts</div>
                                <ul>
                                    <?php foreach( $related_posts as $related_post):  ?>
                                        <li>
                                            <a href="<?php the_permalink($related_post->ID); ?>">
                                                <div class="thumbnail square" style="background: url('<?php echo get_the_post_thumbnail_url($related_post->ID)?>') no-repeat center center"></div>
                                                <?php /* echo $creator->post_title */ ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>



                </div>


                </div>

                <footer class="entry-footer">

                </footer>
            </article>
            
            <?php endwhile; // End of the loop. ?>

        </main>
    </div>

<?php
get_sidebar();
get_footer();
