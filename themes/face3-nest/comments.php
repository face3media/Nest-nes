<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Face3_Nest
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

                            <div id="comments" class="comments clearfix section">

                                <div class="title"><?=__('Notes / Comments','face3-nest')?></div>

                                <?php
	                            if ( have_comments() ) : ?>
                                <ul>
                                <?php
				                wp_list_comments( array(
                                    'walker' => new face3_nest_comment_walker()
				                ) );
			                    ?>
                                </ul>

                                <?php
                                endif; // have_comments()
                                $comment_form_options = [
                                    'must_log_in' => '',
                                    'logged_in_as' => '',
                                    'comment_notes_before' => '',
                                    'title_reply' => '',
                                    'label_submit' => __('Add a note / comment', 'face3-nest'),
									'comment_field' => '<textarea id="comment" name="comment" aria-required="true"></textarea>',
                                ];
                                comment_form($comment_form_options); ?>

                            </div>
