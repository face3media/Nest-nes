<?php
/**
 * Face3 Nest functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Face3_Nest
 */

if ( ! function_exists( 'face3_nest_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function face3_nest_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Face3 Nest, use a find and replace
		 * to change 'face3-nest' to the name of your theme in all the template files.
		 */
        require get_template_directory() . '/admin/admin-init.php';

        load_theme_textdomain( 'face3-nest', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'face3-nest' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'face3_nest_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'face3_nest_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function face3_nest_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'face3_nest_content_width', 640 );
}
add_action( 'after_setup_theme', 'face3_nest_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function face3_nest_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'face3-nest' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'face3-nest' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'face3_nest_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function face3_nest_scripts() {
	wp_enqueue_style( 'face3-nest-style', get_stylesheet_uri() );
    wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css', array(), '20151215' );
    wp_enqueue_script( 'magnific-popupjs', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array('jquery'), '20151215', false );
    wp_enqueue_script( 'gallerymagnific-popupjs', get_template_directory_uri() . '/js/gallery.js', array('magnific-popupjs'), '20151215', true );

    wp_enqueue_style( 'custom', get_template_directory_uri() . '/css/custom.css', array(), '20151215' );

    wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css', array(), '20151215' );

    wp_enqueue_style( 'video-js.css',  'http://vjs.zencdn.net/6.4.0/video-js.css', array(), '20151215' );



    wp_enqueue_script( 'customjs', get_template_directory_uri() . '/js/custom.js', array('ajax-load-more'), '20151215', true );

    wp_enqueue_script( 'face3-nest-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

    wp_enqueue_script( 'video-js',  'http://vjs.zencdn.net/6.4.0/video.js', array(), '20151215', true );


    wp_enqueue_script( 'pdf-js',  'https://cdn.jsdelivr.net/npm/pdfjs-dist@2.0.173/build/pdf.min.js', array(), '20151215', false );

    wp_enqueue_script( 'face3-nest-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
}
add_action( 'wp_enqueue_scripts', 'face3_nest_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_action('um_after_login_fields', 'Add_Azure_login', 1002);
function Add_Azure_login( $args ){
    global $ultimatemember;

  $aad=  AADSSO::get_instance();


    if ( ! session_id() ) {
        session_start();
    }
  /* if (!isset($_SESSION['aadsso_antiforgery-id'])) {
       $antiforgery_id = com_create_guid();
       $_SESSION['aadsso_antiforgery-id'] = $antiforgery_id;
       error_log( "Funtions AAdSSO antiforgery has been set $antiforgery_id");
   }



    // var_dump($_SESSION['aadsso_antiforgery-id']);
    $link= AADSSO_AuthorizationHelper::get_authorization_url( $aad->settings, $_SESSION['aadsso_antiforgery-id'] );

  */
    ?>
    <div class="um-col-alt">







    <div class="um-center">
        <a href="<?php echo $aad->get_login_url(); ?>" class="azure-button" >Sign in with Azure</a>
        <div class="um-clear"></div>
    </div>

   <!-- <div class="um-center">
        <a href="<?php  //echo $aad->get_logout_url(); ?>" class="azure-button" >Logout From Azure</a>
    </div> -->

    </div>

    <?php
}

function get_influences_for_creator($id)
{
    global $wpdb;
    $table_postmeta= $wpdb->prefix."postmeta";
    $table_posts= $wpdb->prefix."posts";
    $length= strlen($id);
    $search_term="s:$length:\"$id\";";
    $query = "SELECT $table_postmeta.post_id FROM $table_postmeta, $table_posts where $table_posts.ID= $table_postmeta.post_id and $table_posts.post_type='influence' and  $table_postmeta.meta_key='creators' and $table_postmeta.meta_value like '%$search_term%'";
    $results=$wpdb->get_col($query,0);



    return $results;
}


function get_reports_for_creator($id)
{
    global $wpdb;
    $table_postmeta= $wpdb->prefix."postmeta";
    $table_posts= $wpdb->prefix."posts";
    $length= strlen($id);
    $search_term="s:$length:\"$id\";";
    $query = "SELECT $table_postmeta.post_id FROM $table_postmeta, $table_posts where $table_posts.ID= $table_postmeta.post_id and $table_posts.post_type='report' and  $table_postmeta.meta_key='creators' and $table_postmeta.meta_value like '%$search_term%'";
    $results=$wpdb->get_col($query,0);



    return $results;
}


function get_collaborators_for_creator($id)
{
    global $wpdb;
    $table_postmeta= $wpdb->prefix."postmeta";
    $table_posts= $wpdb->prefix."posts";
    $length= strlen($id);
    $search_term="s:$length:\"$id\";";
    $query = "SELECT $table_postmeta.post_id FROM $table_postmeta, $table_posts where $table_posts.ID= $table_postmeta.post_id and $table_posts.post_type='creator' and  $table_postmeta.meta_key='collaborators' and $table_postmeta.meta_value like '%$search_term%'";
    $results=$wpdb->get_col($query,0);


    return $results;
}

function get_reports_for_influence($id)
{
    global $wpdb;
    $table_postmeta= $wpdb->prefix."postmeta";
    $table_posts= $wpdb->prefix."posts";
    $length= strlen($id);
    $search_term="s:$length:\"$id\";";
    $query = "SELECT $table_postmeta.post_id FROM $table_postmeta, $table_posts where $table_posts.ID= $table_postmeta.post_id and $table_posts.post_type='report' and  $table_postmeta.meta_key='influences' and $table_postmeta.meta_value like '%$search_term%'";
    $results=$wpdb->get_col($query,0);



    return $results;
}


function sess_start() {
    if (!session_id())
        session_start();
}
add_action('init','sess_start');



function my_custom_featured_image_column_image( $image ) {
    if ( !has_post_thumbnail() )
        return trailingslashit( get_stylesheet_directory_uri() ) . 'images/no-featured-image';
}
add_filter( 'featured_image_column_default_image', 'my_custom_featured_image_column_image' );


 function add_acf_columns ( $columns ) {
    return array_merge ( $columns, array (
        'city' => __ ( 'City' ),
        'country'   => __ ( 'Country' )
    ) );
}
add_filter ('manage_posts_columns', 'add_acf_columns' );

function exhibition_custom_column ( $column, $post_id ) {
    switch ( $column ) {
        case 'city':
            echo get_post_meta ( $post_id, 'city', true );
            break;
        case 'country':
            echo get_post_meta ( $post_id, 'country', true );
            break;
    }
}
add_action ( 'manage_posts_custom_column', 'exhibition_custom_column', 10, 2 );

function get_related_posts_by_tag($post_id, $limit = 5)
{
    $related= array();

    $tags = wp_get_post_tags($post_id);
    if ($tags) {
        $first_tag = $tags[0]->term_id;
        $tagid=array();
        foreach ($tags as $tag)
        {
            $tagid[]=$tag->term_id;
        }
        $args = array(
            'post_type' => array('influence','creator','report'),
            'tag__in' => $tagid,
            'posts_per_page' => $limit,
            'orderby'        => 'rand',
            'post__not_in'  => array($post_id)
        );
        $my_query = new WP_Query($args);
        if ($my_query->have_posts()) {
            while ($my_query->have_posts()) :  $my_query->the_post();
                $related[] = $my_query->post;


            endwhile;
        }
        wp_reset_query();
    }
    return $related;
}

function nest_add_custom_types( $query ) {
    if( (is_tag()  ) && $query->is_main_query() && !is_admin() ) {

        // this gets all post types:
      // $post_types = get_post_types();

        // alternately, you can add just specific post types using this line instead of the above:
        // $post_types = array( 'post', 'your_custom_type' );

        $query->set( 'post_type', array('influence','creator','report') );
    }
}
add_filter( 'pre_get_posts', 'nest_add_custom_types' );


function check_for_video()
{

     $video_extensions = ',mov,m4v,m2v,avi,mpg,flv,wmv,mkv,webm,ogv,mxf,asf,vob,mts,qt,mpeg,x-msvideo,3gp';


     $audio_extensions = ',wma,ogg,wav,m4a';

}

function my_theme_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
}

add_filter( 'get_the_archive_title', 'my_theme_archive_title' );



function get_letter_lists($terms)
{

    global $alpha_chunks;

    $alpha_chunks = array();



    $output = array_map(function ($object) { return $object; }, $terms);

    $atoz = array_merge(array('0','1','2','3','4','5','6','7','8','9'), range('A', 'z'));
    foreach($atoz as $letter) {
        # $array_to_walk isn't defined here, but this is should be the array you'd like to split.
        array_walk($output, 'chunkNames', $letter);
    }

    return $alpha_chunks;
}
function chunkNames(&$value, $key, $letter) {
    global $alpha_chunks;

    if ($value->name[0] === $letter ) {
        $alpha_chunks[$letter][$key] = $value;
    }
    else
    {
        // $alpha_chunks[$letter][$key] = null;
    }
}


/*

This code sample shows you how to use the API to create
and add custom notifications (for real-time notifications)
plugin.

STEP 1: You need to extend the filter: um_notifications_core_log_types with your
new notification type as follows for example

*/

add_filter('um_notifications_core_log_types', 'add_custom_notification_type', 200 );
function add_custom_notification_type( $array ) {

    $array['new_action'] = array(
        'title' => 'Share', // title for reference in backend settings
        'template' => '<strong>{member}</strong> has just shared something with you.', // the template, {member} is a tag, this is how the notification will appear in your notifications
        'account_desc' => 'Share Post or list', // title for account page (notification settings)
    );



    return $array;
}

/*

STEP 2: Add an icon and color to this new notification type

*/

add_filter('um_notifications_get_icon', 'add_custom_notification_icon', 10, 2 );
function add_custom_notification_icon( $output, $type ) {

    if ( $type == 'new_action' ) { // note that our new action id is "new_action" from previous filter
        $output = '<i class="um-icon-android-person-add" style="color: #336699"></i>';
    }

    return $output;
}

/*

STEP 3: Now you just need to add the notification trigger when a user does some action on
another user profile, I assume you can trigger that in some action hooks
for example when user view another user profile you can hook like this

basically you need to run this in code

$who_will_get_notification : is user ID who will get notification
'new_action' is our new notification type
$vars is array containing the required template tags, user photo and url when that notification is clicked

$um_notifications->api->store_notification( $who_will_get_notification, 'new_action', $vars );

*/

add_action('wp_head', 'trigger_new_notification', 100);
function trigger_new_notification( $args ) {

    global $um_notifications;




        um_fetch_user( get_current_user_id() );

        $vars['photo'] = um_get_avatar_url( get_avatar( get_current_user_id(), 40 ) );
        $vars['member'] = "Another User";
        $vars['notification_uri'] = um_user_profile_url();

        $um_notifications->api->store_notification( um_profile_id(), 'new_action', $vars );



}

if ( ! class_exists('face3_nest_comment_walker') ) :
	class face3_nest_comment_walker extends Walker_Comment {

	    function html5_comment($comment, $depth, $args ) {
            ?>
            <li>
                <div class="author"><?php comment_author(); ?><span> <?php comment_date('d / m / Y') ?></span></div>
                <?php comment_text() ?>
            </li>
        <?php
		}

	}
endif;