<?php
add_action('wp_enqueue_scripts', function() {
	wp_enqueue_script('nest-users');
	wp_enqueue_script('nest-user-suggest');
});
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<h1>Users</h1>

			<form>
				<div class="ui-widget">
					<input placeholder="Search for name..." class="nest-suggest-user" name="shareuser" data-autocomplete-field="user_login">
				</div>
				<div class="ui-widget">
					<input placeholder="Search for email..." class="nest-suggest-user" name="shareemail" data-autocomplete-field="user_email">
				</div>
			</form>
<style>
	.ui-autocomplete {
		background:white;
		border:1px solid #ccc;
	}
</style>
			<h1>My lists</h1>

			<?php
			// get user lists
			$my_lists = get_posts([
				'author' => get_current_user_id(),
				'post_type' => 'playlist'
			]);
			echo '<ul id="nest_playlists">';
			foreach ($my_lists as $a_list ) {
				echo '<li data-playlist-id="',$a_list->ID,'">',apply_filters( 'single_post_title', $a_list->post_title, $a_list ),' <i class="fa fa-trash-o"></i></li>';
			}
			echo '</ul>';
			?>

			<form id="nest_create_playlist">
				<?php wp_nonce_field(); ?>
				<input type="hidden" name="action" value="create_playlist">
				<label for="playlist_name">Create new list:</label>
				<input type="text" id="playlist_name" name="playlist_name" placeholder="My list's name">
			</form>
			<form id="nest_delete_playlist">
				<?php wp_nonce_field(); ?>
				<input type="hidden" name="action" value="delete_playlist">
				<input type="hidden" name="playlist_id" value="">
			</form>
			<script>
				jQuery(document).ready(function($) {
					$('#nest_create_playlist').on('submit', function(e) {
						e.preventDefault();

						var data = $(this).serialize();

						$.post(
							ajaxurl,
							data,
							function ( response ) {
								if ( ! response.success ) {
									alert( response.data.message );
								}
								$('#nest_playlists').prepend(
									$('<li data-playlist-id="' + response.data.id + '">' + response.data.playlist_name + ' <i class="fa fa-trash-o"></i></li>')
								);

								$('#playlist_name').val("");
							}
						);
					});
					$('#nest_playlists').on('click', '.fa-trash-o', function(e) {
						e.preventDefault();

						$('#nest_delete_playlist').find('input[name=playlist_id]').first().val($(this).parent().data('playlist-id'));

						var data = $('#nest_delete_playlist').serialize();

						$.post(
							ajaxurl,
							data,
							function ( response ) {
								if ( ! response.success ) {
									alert( response.data.message );
								}
								$('#nest_playlists').find('li[data-playlist-id="' + response.data.id + '"]').first().remove();
							}
						);
					});
				});

			</script>

			<h3>Latest viewed</h3>
			<div><?php echo do_shortcode('[recently_viewed]'); ?></div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
