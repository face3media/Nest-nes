(function($) {
    jQuery('.gallery a').magnificPopup({
        callbacks: {
            elementParse: function(item) {
                // the class name
                if(item.el.context.className == 'video-link') {
                  //  item.type = 'iframe';
                    item.type = 'inline';
                 /*   var itemem= item.el;
                    var siblings = $(itemem).siblings('.player-hidden');
                    var player = $(siblings[0]).html();
                    item.src=player;*/
                } else {
                    item.type = 'image';
                }
            }
        },
        gallery:{enabled:true},
        type: 'image',
    });

})(jQuery);