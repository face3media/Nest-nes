;(function($) {
    
    $('.main-navigation .menu-btn').click(function () {
        $('.main-navigation').toggleClass("open");
    });

    $('.main-navigation .search-btn').click(function () {
        $('.site-search').toggleClass("open");
    });

    function adjustGrid() {

        var column_width = $('.grid .col').outerWidth();
        var grid_width = $('.grid').outerWidth();
        var column_count = grid_width / column_width;
        var column_count_rounded = Math.floor(column_count);
        var columns_total_width = column_count_rounded * column_width;

        var gutter_total_width = gutter * (column_count_rounded - 1);
        var total_width = gutter_total_width + columns_total_width + 100;
        var gutter = Math.floor((grid_width - column_width) / (column_count_rounded - 1));

        $('.grid .col').each(function () {
            $(this).css('margin-right', gutter + "px");
            $(this).css('margin-bottom', gutter + "px");
        });

        $(".grid .col:nth-child(" + column_count_rounded + "n)").css('margin-right', 0);

        if (cols > 2) {

            $('.grid .col').each(function () {
                $(this).css('margin-right', gutter + "px");
                $(this).css('margin-bottom', gutter + "px");
            });

            $(".grid .col:nth-child(" + cols + "n)").css('margin-right', 0);

        } else if (cols == 2) {

            $('.grid .col').each(function () {
                $(this).css('max-width', 50 + '%');
                $(this).css('margin-right', gutter + "px");
                $(this).css('margin-bottom', gutter + "px");
                $(this).height($(this).width());
            });

        } else {

            $('.grid .col').each(function () {
                $(this).css('max-width', 100 + '%');
                $(this).css('margin-right', gutter + "px");
                $(this).css('margin-bottom', gutter + "px");
                $(this).height($(this).width());
            });

        }

    }
    /* export */
    window.adjustGrid = adjustGrid;
    
    
    function adjustWidths() {
        
        var nav_width = $('.site-header').width() - $('.site-branding').width() - 15;
        var foot_width = $('.site-header').width();
        
        $('.main-navigation').css('max-width', nav_width + 'px');
        $('.footer-menu').css('max-width', foot_width + 'px');
        
        if ($(window).width() < 960) {
            var mobile_menu_pos = ( $('body').width() - $('.site-header').width() ) / 2;
            $('.main-navigation').css('right', mobile_menu_pos + 'px');
            $('.filters').css('left', mobile_menu_pos + 'px');
        } else {
            $('.main-navigation').css('right', 0 + 'px');
            $('.filters').css('left', 'inherit');
        }
        
    }
    
    
    function setLastelemtRow() {

        console.log('setting last element row');
        var colArray= $('.grid .col');
        var previous= colArray[0];
        var before_previous= colArray[0];

        var rowtop=$(previous).position().top;
        colArray.each(function () {
            var position= $(this).position();

            if (position.top > rowtop)
            {
                $(previous).addClass('last-ele-in-row');
                $(before_previous).addClass('before-last-ele-in-row');

                before_previous= previous;
                previous = $(this);
                rowtop=$(previous).position().top;
            }
            else
            {
                before_previous= previous;
                previous = $(this);
                $(this).removeClass( "before-last-ele-in-row" );

                $(this).removeClass( "last-ele-in-row" );
            }
        });
        
        adjustAjaxGrid();

    } 

    function adjustAjaxGrid() {
        $('#content > .grid').append( $('.alm-reveal .col-1-6') );
        $('#content > .grid').append( $('.ajax-load-more-wrap') );
    }

    $.fn.almDestroyed = function(alm){
        $('#pagination > div.page_next').addClass('show');
    };

    $.fn.almDone = function(){
        $('#pagination > div.page_next').removeClass('show');
    };

    $(window).resize(function () {

        $('.grid .col').each(function () {
            $(this).height($(this).width());
        });

        $('.single .details .thumbnail').each(function () {
            $(this).height($(this).width());
        });

        $('.single .gallery ul li').each(function () {
            $(this).height($(this).width());
        });

        /* adjustGrid(); */
        adjustWidths();
        setLastelemtRow();

    }).resize();



    $(document).ready(function () {

        // Change IMG to SVG for color change
        $('img[src$=".svg"]').each(function () {
            var $img = jQuery(this);
            var imgURL = $img.attr('src');
            var attributes = $img.prop("attributes");

            $.get(imgURL, function (data) {
                var $svg = jQuery(data).find('svg');
                $svg = $svg.removeAttr('xmlns:a');
                $.each(attributes, function () {
                    $svg.attr(this.name, this.value);
                });
                $img.replaceWith($svg);
            }, 'xml');
        });

        /* adjustGrid(); */
        adjustWidths();

    });


})(jQuery);