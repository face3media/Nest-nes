<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Face3_Nest
 */
$Influences_objects = get_field('influences');

$creators_objects = get_field('creators');
$gallery = get_field('gallery');


get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post();
				do_action('output_ajax_pageview', get_the_ID());?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <div class="entry-content">
                   
                    <div class="details">
                       
                        <div class="title">
                        <div class="category">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ico-report.svg" alt="<?php echo ucfirst(get_post_type())?>" />
                        </div>

                        <?php the_title( '<h1>', '</h1>' ); ?>
                        </div>
                        
                        <a href="#" class="download" target="_blank"><i class="fa fa-download ease" aria-hidden="true"></i>Download Report</a>
                       
                        <div class="profile">
                            <div class="thumbnail square" style="background: url('http://www.dimitribaehler.ch/img/uploads/2013/01/dimitribaehler_xjcx_1-1.jpg') no-repeat;"></div>
                            <ul class="icons">
                                <li><i data-post-id="<?= get_the_ID();?>" class="fa <?= \Face3\Nest\Filters\frontend\is_liked() ? 'fa-heart' : 'fa-heart-o'?>" aria-hidden="true"></i>&nbsp;<?= intval(get_post_meta(get_the_ID(), FACE3_NEST_FILTERS_FAVOURITE_COUNT_META_KEY, true))?></li>
                                <li><?=do_shortcode('[add_to_playlist]')?></li>
                                <li><i class="fa fa-upload" aria-hidden="true"></i></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                    </div>

                    <div class="gallery">
                        <?php get_template_part( 'template-parts/content', 'gallery' ); ?>
                    </div>


                    <div class="related clearfix section">
                       

                        <?php
                        $related_posts = get_related_posts_by_tag(get_the_id(),20);

                        if( $Influences_objects|| $creators_objects):
                        ?>
                          <strong class="title">Related Profiles</strong>
                             <div class="profiles">

                               <ul>
                                     <?php foreach( $Influences_objects as $influence):  ?>
                                             <li style="background: url('<?php echo get_the_post_thumbnail_url($influence->ID,'thumbnail')?>') no-repeat center center">
                                                  <a href="<?php the_permalink($influence->ID); ?>">
                                                 </a>
                                              </li>
                                <?php endforeach; ?>
                                   <?php foreach( $creators_objects as $creator):  ?>
                                       <li style="background: url('<?php echo get_the_post_thumbnail_url($creator->ID,'thumbnail')?>') no-repeat center center">
                                           <a href="<?php the_permalink($creator->ID); ?>">
                                           </a>
                                       </li>
                                   <?php endforeach; ?>

                               </ul>
                             </div>

                        <?php endif; ?>
                        

                        


                        <?php $posttags = get_the_tags();?>
                        <?php if( $posttags ): ?>

                            <div class="keywords">
                                <strong>Keywords</strong>
                                <ul>
                                    <?php foreach (get_the_tags() as $tag):?>
                                        <li>
                                            <a href="<?php  get_tag_link($tag->term_id) ?>">
                                                <?php echo $tag->name; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                    </div>


                </div>

                <footer class="entry-footer">

                </footer>
            </article>

            <?php endwhile; // End of the loop. ?>

		</main>
	</div>

<?php
get_sidebar();
get_footer();
