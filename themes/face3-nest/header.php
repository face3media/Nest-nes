<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Face3_Nest
 */
global $redux_builder_nest;
$logo = $redux_builder_nest['nest-logo-white']['url'];

?>
<!doctype html>
<html <?php language_attributes(); ?> style="margin-top: 0 !important;">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- script src="https://code.jquery.com/jquery-3.2.1.min.js"></script -->
    
    <script src="https://use.typekit.net/kkd8nhk.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <script src="https://use.fontawesome.com/c18b885bc8.js"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'face3-nest' ); ?></a>

	<header id="masthead" class="site-header">
        
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php// bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php //bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
            
            <ul class="menu-full">
                <?php echo do_shortcode('[ultimatemember_notice]'); ?>
                <li class="mobile">
                    <div class="profile-btn"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; <a style="display: inline-block" href="<?php echo  get_permalink( get_page_by_path( 'my-account' ) ); ?>"><?php echo um_user( 'display_name' );?></a></div>
                </li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">Logout</a></li>
                <li class="mobile">
                    <div class="site-search">
                        <?php echo do_shortcode('[wi_autosearch_suggest_form]'); ?>
                        <?php //get_search_form() ?>
                    </div>
                </li>
                <li>
                    <button type="button" class="menu-btn">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </li>
                
            </ul>
            
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
        
            <!-- <div class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></div> -->
            <div class="profile-btn desktop"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; <a style="display: inline-block" href="<?php echo  get_permalink( get_page_by_path( 'my-account' ) ); ?>"><?php echo um_user( 'display_name' );?></a></div>
           
            <div class="site-search desktop">
                <?php echo do_shortcode('[wi_autosearch_suggest_form]'); ?>
                <?php //get_search_form() ?>
            </div>
            
            
            
		</nav><!-- #site-navigation -->
		
        <div class="filters">

            <ul class="categories">
                <li><a class="active" href="<?php echo  home_url()?>">All</a>&nbsp;&nbsp;/&nbsp;</li>
                <li><a href="<?php echo get_post_type_archive_link('creator')?>">Creators</a>&nbsp;&nbsp;/&nbsp;</li>
                <li><a href="<?php echo get_post_type_archive_link('influence')?>">Influences</a>&nbsp;&nbsp;/&nbsp;</li>
                <li><a href="<?php echo get_post_type_archive_link('report')?>">Reports</a></li>
            </ul>

            <?php if(is_front_page() || is_archive() ):?>
            <div style="display:inline-block; text-align:left; margin-top: 15px;">
           
                <div class="tags dropdown">
                    Tags&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>

                    <ul>
                        <?php 
                        $tags = get_tags();
                        $letter_list= get_letter_lists($tags);
                        foreach ( $letter_list as $letter => $tag_array ) {
                            echo "<li><a href='javascript:void(0);' title='$letter' class='tag-letter'>$letter</a></li>";
                            foreach ($tag_array as $tag) {
                               $tag_link = get_tag_link($tag->term_id);
                                echo "<li><a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
                                echo "{$tag->name}</a></li>";
                            }
                        }
                        ?>
                    </ul>

                </div>

                <div class="order dropdown">
                    Filter by&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>
                    <ul>
                        <li><a href="<?= add_query_arg( 'filter', 'recent' ); ?>">Most Recent</a></li>
                        <li><a href="<?= add_query_arg( 'filter', 'liked' ); ?>">Most Appreciated</a></li>
                        <li><a href="<?= add_query_arg( 'filter', 'commented' ); ?>">Most Discussed</a></li>
                    </ul>
                </div>
                
            </div>

            <?php endif;?>

        </div>
		
	</header><!-- #masthead -->

	<div id="content" class="site-content">