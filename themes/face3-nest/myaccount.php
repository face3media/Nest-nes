<?php
add_action('wp_enqueue_scripts', function() {
    wp_enqueue_script('nest-users');
    wp_enqueue_script('nest-user-suggest');
});
get_header();

?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            
            <div class="user">
            
                <div class="title">
                    <i class="fa fa-user" aria-hidden="true"></i>&nbsp;<h1>Mathieu Chalifoux</h1>
                    <a href="#" class="edit-profile" target="_blank"><i class="fa fa-cog ease" aria-hidden="true"></i>Edit Profile</a>
                </div>
                
            </div>

            <div style="display: none;">
                <h1>Users</h1>

                <form>
                    <div class="ui-widget">
                        <input placeholder="Search for name..." class="nest-suggest-user" name="shareuser" data-autocomplete-field="user_login">
                    </div>
                    <div class="ui-widget">
                        <input placeholder="Search for email..." class="nest-suggest-user" name="shareemail" data-autocomplete-field="user_email">
                    </div>
                </form>
                <style>
                    .ui-autocomplete {
                        background:white;
                        border:1px solid #ccc;
                    }
                </style>
            </div>

            <div style="display: none;">
                <h1>My lists</h1>

                <?php
                // get user lists
                $my_lists = get_posts([
                    'author' => get_current_user_id(),
                    'post_type' => 'playlist'
                ]);
                echo '<ul id="nest_playlists">';
                foreach ($my_lists as $a_list ) {
                    echo '<li data-playlist-id="',$a_list->ID,'">',apply_filters( 'single_post_title', $a_list->post_title, $a_list ),' <i class="fa fa-trash-o"></i></li>';
                }
                echo '</ul>';
                ?>
            </div>
            
            <div class="gallery">
                
                <div class="col-1-3" id="mylist">
                        
                    <div class="tabs">
                        <h3 id="mylists_btn" class="tablink active" onclick="openTab(event, 'mylists')">My Lists</h3>
                        <h3 id="newlist_btn" class="tablink" onclick="openTab(event, 'createlist')"><i class="fa fa-plus ease" aria-hidden="true"></i>Create New List</h3>
                    </div>
                        
                    <div class="lists" id="mylists">
                        <ul class="grid">
                            <li class="col col-1-6" style="background: url(&quot;http://nest-dev.azurewebsites.net/wp-content/uploads/2017/11/circus-de-soleil-performers-artists-cast-of-Zaia-cirque-du-soleil-london-based-Spark-Fire-corporate-entertainers.jpg&quot;) center center no-repeat; height: 207px;">
                                <div class="name">List 01</div>
                                <i class="fa fa-trash-o ease"></i>
                            </li>
                            <li class="col col-1-6" style="background: url(&quot;http://nest-dev.azurewebsites.net/wp-content/uploads/2017/11/circus-de-soleil-performers-artists-cast-of-Zaia-cirque-du-soleil-london-based-Spark-Fire-corporate-entertainers.jpg&quot;) center center no-repeat; height: 207px;">
                                <div class="name">List 02</div>
                                <i class="fa fa-trash-o ease"></i>
                            </li>
                            <li class="col col-1-6" style="background: url(&quot;http://nest-dev.azurewebsites.net/wp-content/uploads/2017/11/circus-de-soleil-performers-artists-cast-of-Zaia-cirque-du-soleil-london-based-Spark-Fire-corporate-entertainers.jpg&quot;) center center no-repeat; height: 207px;">
                                <div class="name">List 03</div>
                                <i class="fa fa-trash-o ease"></i>
                            </li>
                            <li class="col col-1-6" style="background: url(&quot;http://nest-dev.azurewebsites.net/wp-content/uploads/2017/11/circus-de-soleil-performers-artists-cast-of-Zaia-cirque-du-soleil-london-based-Spark-Fire-corporate-entertainers.jpg&quot;) center center no-repeat; height: 207px;">
                                <div class="name">List 04</div>
                                <i class="fa fa-trash-o ease"></i>
                            </li>
                            <li class="col col-1-6" style="background: url(&quot;http://nest-dev.azurewebsites.net/wp-content/uploads/2017/11/circus-de-soleil-performers-artists-cast-of-Zaia-cirque-du-soleil-london-based-Spark-Fire-corporate-entertainers.jpg&quot;) center center no-repeat; height: 207px;">
                                <div class="name">List 05</div>
                                <i class="fa fa-trash-o ease"></i>
                            </li>
                        </ul>
                    </div>
                        
                    <div class="lists" id="createlist" style="display:none">
                        
                        <form id="nest_create_playlist">
                            <?php wp_nonce_field(); ?>
                            <input type="hidden" name="action" value="create_playlist">
                            <input type="text" id="playlist_name" name="playlist_name" placeholder="New List Name">
                        </form>
                        <form id="nest_delete_playlist">
                            <?php wp_nonce_field(); ?>
                            <input type="hidden" name="action" value="delete_playlist">
                            <input type="hidden" name="playlist_id" value="">
                        </form>
                        
                        <?php
                        // get user lists
                        $my_lists = get_posts([
                            'author' => get_current_user_id(),
                            'post_type' => 'playlist'
                        ]);
                        echo '<ul id="nest_playlists" class="ease">';
                        foreach ($my_lists as $a_list ) {
                            echo '<li data-playlist-id="',$a_list->ID,'">',apply_filters( 'single_post_title', $a_list->post_title, $a_list ),' <i class="fa fa-trash-o ease"></i></li>';
                        }
                        echo '</ul>';
                        ?>
                    </div>
                    
                    <script>

                    function openTab(evt, tabName) {
                      var i, x, tablinks;
                      x = document.getElementsByClassName("lists");
                      for (i = 0; i < x.length; i++) {
                          x[i].style.display = "none";
                      }
                      tablinks = document.getElementsByClassName("tablink");
                      for (i = 0; i < x.length; i++) {
                          tablinks[i].className = tablinks[i].className.replace(" active", "");
                      }
                      document.getElementById(tabName).style.display = "block";
                      evt.currentTarget.className += " active";
                    }
                        
                    </script>
                    
                </div>

                <div class="col-1-3" id="favourites">
                    <h3><i class="fa fa-heart-o" aria-hidden="true"></i>&nbsp;Favourites</h3>
                    <div><?php echo do_shortcode('[recently_viewed]'); ?></div>
                </div>

                <div class="col-1-3" id="latest">
                    <h3><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;Latest viewed</h3>
                    <div><?php echo do_shortcode('[recently_viewed]'); ?></div>
                </div>
                
            </div>
            
            <script>
                jQuery(document).ready(function($) {
                    $('#nest_create_playlist').on('submit', function(e) {
                        e.preventDefault();

                        var data = $(this).serialize();

                        $.post(
                            ajaxurl,
                            data,
                            function ( response ) {
                                if ( ! response.success ) {
                                    alert( response.data.message );
                                }
                                $('#nest_playlists').prepend(
                                    $('<li data-playlist-id="' + response.data.id + '">' + response.data.playlist_name + ' <i class="fa fa-trash-o"></i></li>')
                                );

                                $('#playlist_name').val("");
                            }
                        );
                    });
                    $('#nest_playlists').on('click', '.fa-trash-o', function(e) {
                        e.preventDefault();

                        $('#nest_delete_playlist').find('input[name=playlist_id]').first().val($(this).parent().data('playlist-id'));

                        var data = $('#nest_delete_playlist').serialize();

                        $.post(
                            ajaxurl,
                            data,
                            function ( response ) {
                                if ( ! response.success ) {
                                    alert( response.data.message );
                                }
                                $('#nest_playlists').find('li[data-playlist-id="' + response.data.id + '"]').first().remove();
                            }
                        );
                    });
                });

            </script>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
